﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CVI_V3.ValidationRules
{
    class Div10R0R255 : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
                return new ValidationResult(false, "格式錯誤");

            double t;
            try
            {
                t = Convert.ToDouble(value.ToString());
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "格式錯誤");
            }
            if (t > 25.5 || t < 0)
                return new ValidationResult(false, "0 ~ 25.5");

            return new ValidationResult(true, null);
        }
    }
}
