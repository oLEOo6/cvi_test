﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CVI_V3.ValidationRules
{
    class Div1000R0R65535 : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
                return new ValidationResult(false, "格式錯誤");

            double t;
            try
            {
                t = Convert.ToDouble(value.ToString());
            }catch(Exception e)
            {
                return new ValidationResult(false, "格式錯誤");
            }
            if(t>65.535 || t < 0)
                return new ValidationResult(false, "0 ~ 65.535");

            return new ValidationResult(true, null);
        }
    }
}
