﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CVI_V3.ValidationRules
{
    class Div100Rn32768R32767 : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
                return new ValidationResult(false, "格式錯誤");

            double t;
            try
            {
                t = Convert.ToDouble(value.ToString());
            }catch(Exception e)
            {
                return new ValidationResult(false, "格式錯誤");
            }
            if(t>327.67 || t < -327.68)
                return new ValidationResult(false, "-327.68 ~ 327.67");

            return new ValidationResult(true, null);
        }
    }
}
