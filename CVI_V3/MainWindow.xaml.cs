﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_V3
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            //限制輸入僅能0 - 9
            if (!Regex.IsMatch(e.Text, "^[0-9]$"))
            {
                e.Handled = true;
                return;
            }
            ////確認值大於0
            //if (Convert.ToInt32(e.Text) > 0)
            e.Handled = false;
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }


        private void MAIN_Button_Click(object sender, RoutedEventArgs e)
        {
            MAIN.Visibility = Visibility.Visible;
            EE.Visibility = Visibility.Collapsed;
            Calibratoin.Visibility = Visibility.Collapsed;
            Settings.Visibility = Visibility.Collapsed;
        }
        private void EE_Button_Click(object sender, RoutedEventArgs e)
        {
            MAIN.Visibility = Visibility.Collapsed;
            EE.Visibility = Visibility.Visible;
            Calibratoin.Visibility = Visibility.Collapsed;
            Settings.Visibility = Visibility.Collapsed;
        }
        private void Calibratoin_Button_Click(object sender, RoutedEventArgs e)
        {
            MAIN.Visibility = Visibility.Collapsed;
            EE.Visibility = Visibility.Collapsed;
            Calibratoin.Visibility = Visibility.Visible;
            Settings.Visibility = Visibility.Collapsed;
        }
        private void Settings_Button_Click(object sender, RoutedEventArgs e)
        {
            MAIN.Visibility = Visibility.Collapsed;
            EE.Visibility = Visibility.Collapsed;
            Calibratoin.Visibility = Visibility.Collapsed;
            Settings.Visibility = Visibility.Visible;
        }
    }

    
}
