﻿using CVI_Lib;
using CVI_V3.Models;
using Laike.Can.CanCmd;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
//using System.Windows.Forms;
using System.Windows.Input;

namespace CVI_V3.ViewModels
{
    class MainWindowVM : ViewModelBase
    {
        #region Parameter Settings
        public static int PACK;
        public static int CELL;
        public static int TEMP;
        public static String CAN_SPEED;
        public static String LOG_PATH;

        public int PACK_View { get { return PACK; } set { PACK = value; OnPropertyChanged(); } }
        public int CELL_View { get { return CELL; } set { CELL = value; OnPropertyChanged(); } }
        public int TEMP_View { get { return TEMP; } set { TEMP = value; OnPropertyChanged(); } }
        public String CAN_SPEED_View { get { return CAN_SPEED; } set { CAN_SPEED = value; OnPropertyChanged(); } }
        public String LOG_PATH_View { get { return LOG_PATH; } set { LOG_PATH = value; OnPropertyChanged(); } }
        #endregion

        #region Field
        private Battery battery = new Battery();
        public Battery Battery { get { return battery; } set { battery = value;OnPropertyChanged(); } }
        Package[] Package;//Run ini

        private ObservableCollection<Package> obs = new ObservableCollection<Package>();
        public ObservableCollection<Package> OBS { get { return obs; } set { obs = value;OnPropertyChanged(); } }
        #endregion

        #region EE
        EEPROM _Rated_Capacity = new EEPROM(0x00, 2, "Rated Capacity", "Ah");
        EEPROM _Nominal_Voltage = new EEPROM(0x02, 2, "Nominal Voltage", "V");
        EEPROM _Packs_EE = new EEPROM(0x04, 1, "Packs", "");
        EEPROM _Cells_EE = new EEPROM(0x05, 1, "Cells", "");
        EEPROM _Temps_EE = new EEPROM(0x06, 1, "Temps", "");
        EEPROM _Current_Model = new EEPROM(0x07, 1, "Current Model", "");
        EEPROM _Firmware_Version = new EEPROM(0x08, 2, "Firmware Version", "");

        EEPROM _ID_L = new EEPROM(0x10, 7, "ID L", "");
        EEPROM _ID_H = new EEPROM(0x18, 4, "ID H", "");

        EEPROM _OV_Tv_1 = new EEPROM(0x20, 2, "OV_Thresh", "V");
        EEPROM _OV_Tv_2 = new EEPROM(0x22, 2, "OV_Thresh_Lv2", "V");
        EEPROM _OV_Tv_3 = new EEPROM(0x24, 2, "OV_Thresh_Lv3", "V");
        EEPROM _OV_Rv_1 = new EEPROM(0x26, 2, "OV_Release", "V");
        EEPROM _OV_Rv_2 = new EEPROM(0x28, 2, "OV_Release_Lv2", "V");
        EEPROM _OV_Rv_3 = new EEPROM(0x2A, 2, "OV_Release_Lv3", "V");
        EEPROM _OV_Dt_1 = new EEPROM(0x2C, 1, "OV_DelayTime", "sec");
        EEPROM _OV_Dt_2 = new EEPROM(0x2D, 1, "OV_DelayTime_Lv2", "sec");
        EEPROM _OV_Dt_3 = new EEPROM(0x2E, 1, "OV_DelayTime_Lv3", "sec");
        EEPROM _UV_Tv_1 = new EEPROM(0x30, 2, "UV_Thresh", "V");
        EEPROM _UV_Tv_2 = new EEPROM(0x32, 2, "UV_Thresh_Lv2", "V");
        EEPROM _UV_Tv_3 = new EEPROM(0x34, 2, "UV_Thresh_Lv3", "V");
        EEPROM _UV_Rv_1 = new EEPROM(0x36, 2, "UV_Release", "V");
        EEPROM _UV_Rv_2 = new EEPROM(0x38, 2, "UV_Release_Lv2", "V");
        EEPROM _UV_Rv_3 = new EEPROM(0x3A, 2, "UV_Release_Lv3", "V");
        EEPROM _UV_Dt_1 = new EEPROM(0x3C, 1, "UV_DelayTime", "sec");
        EEPROM _UV_Dt_2 = new EEPROM(0x3D, 1, "UV_DelayTime_Lv2", "sec");
        EEPROM _UV_Dt_3 = new EEPROM(0x3E, 1, "UV_DelayTime_Lv3", "sec");

        EEPROM _OC_C_Tv_1 = new EEPROM(0x40, 2, "OC_C_Thresh", "A");
        EEPROM _OC_C_Tv_2 = new EEPROM(0x42, 2, "OC_C_Thresh_Lv2", "A");
        EEPROM _OC_C_Tv_3 = new EEPROM(0x44, 2, "OC_C_Thresh_Lv3", "A");
        EEPROM _OC_C_Rt_1 = new EEPROM(0x46, 1, "OC_C_ReleaseTime", "sec");
        EEPROM _OC_C_Rt_2 = new EEPROM(0x47, 1, "OC_C_ReleaseTime_Lv2", "sec");
        EEPROM _OC_C_Rt_3 = new EEPROM(0x48, 1, "OC_C_ReleaseTime_Lv3", "sec");
        EEPROM _OC_C_Dt_1 = new EEPROM(0x49, 1, "OC_C_DelayTime", "sec");
        EEPROM _OC_C_Dt_2 = new EEPROM(0x4A, 1, "OC_C_DelayTime_Lv2", "sec");
        EEPROM _OC_C_Dt_3 = new EEPROM(0x4B, 1, "OC_C_DelayTime_Lv3", "sec");
        EEPROM _OC_D_Tv_1 = new EEPROM(0x50, 2, "OC_D_Thresh", "A");
        EEPROM _OC_D_Tv_2 = new EEPROM(0x52, 2, "OC_D_Thresh_Lv2", "A");
        EEPROM _OC_D_Tv_3 = new EEPROM(0x54, 2, "OC_D_Thresh_Lv3", "A");
        EEPROM _OC_D_Rt_1 = new EEPROM(0x56, 1, "OC_D_ReleaseTime", "sec");
        EEPROM _OC_D_Rt_2 = new EEPROM(0x57, 1, "OC_D_ReleaseTime_Lv2", "sec");
        EEPROM _OC_D_Rt_3 = new EEPROM(0x58, 1, "OC_D_ReleaseTime_Lv3", "sec");
        EEPROM _OC_D_Dt_1 = new EEPROM(0x59, 1, "OC_D_DelayTime", "sec");
        EEPROM _OC_D_Dt_2 = new EEPROM(0x5A, 1, "OC_D_DelayTime_Lv2", "sec");
        EEPROM _OC_D_Dt_3 = new EEPROM(0x5B, 1, "OC_D_DelayTime_Lv3", "sec");

        EEPROM _OT_C_Tv_1 = new EEPROM(0x60, 2, "OT_C_Thresh", "°C");
        EEPROM _OT_C_Tv_2 = new EEPROM(0x62, 2, "OT_C_Thresh_Lv2", "°C");
        EEPROM _OT_C_Tv_3 = new EEPROM(0x64, 2, "OT_C_Thresh_Lv3", "°C");
        EEPROM _OT_C_Rv_1 = new EEPROM(0x66, 2, "OT_C_Release", "°C");
        EEPROM _OT_C_Rv_2 = new EEPROM(0x68, 2, "OT_C_Release_Lv2", "°C");
        EEPROM _OT_C_Rv_3 = new EEPROM(0x6A, 2, "OT_C_Release_Lv3", "°C");
        EEPROM _OT_C_Dt_1 = new EEPROM(0x6C, 1, "OT_C_DelayTime", "sec");
        EEPROM _OT_C_Dt_2 = new EEPROM(0x6D, 1, "OT_C_DelayTime_Lv2", "sec");
        EEPROM _OT_C_Dt_3 = new EEPROM(0x6E, 1, "OT_C_DelayTime_Lv3", "sec");
        EEPROM _OT_D_Tv_1 = new EEPROM(0x70, 2, "OT_D_Thresh", "°C");
        EEPROM _OT_D_Tv_2 = new EEPROM(0x72, 2, "OT_D_Thresh_Lv2", "°C");
        EEPROM _OT_D_Tv_3 = new EEPROM(0x74, 2, "OT_D_Thresh_Lv3", "°C");
        EEPROM _OT_D_Rv_1 = new EEPROM(0x76, 2, "OT_D_Release", "°C");
        EEPROM _OT_D_Rv_2 = new EEPROM(0x78, 2, "OT_D_Release_Lv2", "°C");
        EEPROM _OT_D_Rv_3 = new EEPROM(0x7A, 2, "OT_D_Release_Lv3", "°C");
        EEPROM _OT_D_Dt_1 = new EEPROM(0x7C, 1, "OT_D_DelayTime", "sec");
        EEPROM _OT_D_Dt_2 = new EEPROM(0x7D, 1, "OT_D_DelayTime_Lv2", "sec");
        EEPROM _OT_D_Dt_3 = new EEPROM(0x7E, 1, "OT_D_DelayTime_Lv3", "sec");
        EEPROM _UT_C_Tv_1 = new EEPROM(0x80, 2, "UT_C_Thresh", "°C");
        EEPROM _UT_C_Tv_2 = new EEPROM(0x82, 2, "UT_C_Thresh_Lv2", "°C");
        EEPROM _UT_C_Tv_3 = new EEPROM(0x84, 2, "UT_C_Thresh_Lv3", "°C");
        EEPROM _UT_C_Rv_1 = new EEPROM(0x86, 2, "UT_C_Release", "°C");
        EEPROM _UT_C_Rv_2 = new EEPROM(0x88, 2, "UT_C_Release_Lv2", "°C");
        EEPROM _UT_C_Rv_3 = new EEPROM(0x8A, 2, "UT_C_Release_Lv3", "°C");
        EEPROM _UT_C_Dt_1 = new EEPROM(0x8C, 1, "UT_C_DelayTime", "sec");
        EEPROM _UT_C_Dt_2 = new EEPROM(0x8D, 1, "UT_C_DelayTime_Lv2", "sec");
        EEPROM _UT_C_Dt_3 = new EEPROM(0x8E, 1, "UT_C_DelayTime_Lv3", "sec");
        EEPROM _UT_D_Tv_1 = new EEPROM(0x90, 2, "UT_D_Thresh", "°C");
        EEPROM _UT_D_Tv_2 = new EEPROM(0x92, 2, "UT_D_Thresh_Lv2", "°C");
        EEPROM _UT_D_Tv_3 = new EEPROM(0x94, 2, "UT_D_Thresh_Lv3", "°C");
        EEPROM _UT_D_Rv_1 = new EEPROM(0x96, 2, "UT_D_Release", "°C");
        EEPROM _UT_D_Rv_2 = new EEPROM(0x98, 2, "UT_D_Release_Lv2", "°C");
        EEPROM _UT_D_Rv_3 = new EEPROM(0x9A, 2, "UT_D_Release_Lv3", "°C");
        EEPROM _UT_D_Dt_1 = new EEPROM(0x9C, 1, "UT_D_DelayTime", "sec");
        EEPROM _UT_D_Dt_2 = new EEPROM(0x9D, 1, "UT_D_DelayTime_Lv2", "sec");
        EEPROM _UT_D_Dt_3 = new EEPROM(0x9E, 1, "UT_D_DelayTime_Lv3", "sec");

        EEPROM _SC_Tv = new EEPROM(0xA0, 2, "SC_Thresh", "A");
        EEPROM _SC_Rt = new EEPROM(0xA2, 2, "SC_Release", "sec");
        EEPROM _SC_Dt = new EEPROM(0xA4, 1, "SC_DelayCount", "Times");
        EEPROM _HV_Dt = new EEPROM(0xA6, 1, "HV_DelayTime", "sec");
        EEPROM _LV_Dt = new EEPROM(0xA7, 1, "LV_DelayTime", "sec");
        EEPROM _HV_Cutoff = new EEPROM(0xA8, 2, "HV_Cutoff", "V");
        EEPROM _LV_Cutoff = new EEPROM(0xAA, 2, "LV_Cutoff", "V");
        EEPROM _Temp_UpperLimit = new EEPROM(0xAC, 2, "Temp UpperLimit", "°C");
        EEPROM _Temp_LowerLimit = new EEPROM(0xAE, 2, "Temp LowerLimit", "°C");

        EEPROM _Charging_Voltage = new EEPROM(0xB0, 2, "Charging Voltage", "V");
        EEPROM _Charging_Current = new EEPROM(0xB2, 2, "Charging Current", "A");
        EEPROM _FC_Tv = new EEPROM(0xB4, 2, "FC_Thresh", "V");
        EEPROM _FC_Rv = new EEPROM(0xB6, 2, "FC_Release", "V");
        EEPROM _FC_Dt = new EEPROM(0xB8, 1, "FC_DelayTime", "sec");
        EEPROM _FD_Tv = new EEPROM(0xB9, 2, "FD_Thresh", "V");
        EEPROM _FD_Rv = new EEPROM(0xBB, 2, "FD_Release", "V");
        EEPROM _FD_Dt = new EEPROM(0xBD, 1, "FD_DelayTime", "sec");
        EEPROM _Charge_State = new EEPROM(0xBE, 1, "Charged State", "");
        EEPROM _Charge_TimeOut = new EEPROM(0xBF, 1, "Charged TimeOut", "hr");

        EEPROM _Power_Consumption = new EEPROM(0xC0, 2, "Power Consumption", "A");
        EEPROM _Cyclic_Capacity = new EEPROM(0xC2, 2, "Cyclic Capacity", "Ah");
        EEPROM _Cumulative_Capability = new EEPROM(0xC4, 4, "Cumulative Capability", "Ah");
        EEPROM _Storage_Capability = new EEPROM(0xC8, 4, "Storage Capability", "As");
        EEPROM _Available_Capacity = new EEPROM(0xCC, 4, "Available Capacity", "As");

        EEPROM _Gain_1 = new EEPROM(0xD0, 2, "Gain1", "");
        EEPROM _Offset_1 = new EEPROM(0xD2, 2, "Offset1", "");
        EEPROM _Shunt_1 = new EEPROM(0xD4, 2, "Shunt / Sensitivity 1", "");
        EEPROM _Reverse_Currrent = new EEPROM(0xD6, 2, "Reverse Current", "A");
        EEPROM _Gain_2 = new EEPROM(0xD8, 2, "Gain2", "");
        EEPROM _Offset_2 = new EEPROM(0xDA, 2, "Offset2", "");
        EEPROM _Shunt_2 = new EEPROM(0xDC, 2, "Shunt / Sensitivity 2", "");
        EEPROM _Ignored_Current = new EEPROM(0xDE, 2, "Ignored Current", "A");

        EEPROM _OV_Count = new EEPROM(0xE0, 2, "OV Count", "Times");
        EEPROM _UV_Count = new EEPROM(0xE2, 2, "UV Count", "Times");
        EEPROM _OT_Count = new EEPROM(0xE4, 2, "OT Count", "Times");
        EEPROM _UT_Count = new EEPROM(0xE6, 2, "UT Count", "Times");
        EEPROM _OC_C_Count = new EEPROM(0xE8, 2, "OC Charge Count", "Times");
        EEPROM _OC_D_Count = new EEPROM(0xEA, 2, "OC Discharge Count", "Times");
        EEPROM _SC_Count = new EEPROM(0xEC, 2, "SC Count", "Times");
        EEPROM _Boot = new EEPROM(0xEE, 2, "Boot", "Times");

        EEPROM _CellV_Max = new EEPROM(0xF0, 2, "CellV_Max", "V");
        EEPROM _CellV_Min = new EEPROM(0xF2, 2, "CellV_Min", "V");
        EEPROM _CellT_Max = new EEPROM(0xF4, 2, "CellT_Max", "°C");
        EEPROM _CellT_Min = new EEPROM(0xF6, 2, "CellT_Min", "°C");
        EEPROM _CHG_Cur_Max = new EEPROM(0xF8, 2, "CHG Current Max", "A");
        EEPROM _DSG_Cur_Max = new EEPROM(0xFA, 2, "DSG Current Max", "A");
        EEPROM _SOC_EE = new EEPROM(0xFC, 1, "SOC", "%");
        EEPROM _SOH_EE = new EEPROM(0xFD, 1, "SOH", "%");
        EEPROM _Charge_Cycle = new EEPROM(0xFE, 2, "Charge Cycle", "Cycles");

        public EEPROM Rated_Capacity { get { return _Rated_Capacity; } set { _Rated_Capacity = value; OnPropertyChanged(); } }
        public EEPROM Nominal_Voltage { get { return _Nominal_Voltage; } set { _Nominal_Voltage = value; OnPropertyChanged(); } }
        public EEPROM Packs_EE { get { return _Packs_EE; } set { _Packs_EE = value; OnPropertyChanged(); } }
        public EEPROM Cells_EE { get { return _Cells_EE; } set { _Cells_EE = value; OnPropertyChanged(); } }
        public EEPROM Temps_EE { get { return _Temps_EE; } set { _Temps_EE = value; OnPropertyChanged(); } }
        public EEPROM Current_Model { get { return _Current_Model; } set { _Current_Model = value; OnPropertyChanged(); } }
        public EEPROM Firmware_Version { get { return _Firmware_Version; } set { _Firmware_Version = value; OnPropertyChanged(); } }

        public EEPROM ID_L { get { return _ID_L; } set { _ID_L = value; OnPropertyChanged(); } }
        public EEPROM ID_H { get { return _ID_H; } set { _ID_H = value; OnPropertyChanged(); } }

        public EEPROM OV_Tv_1 { get { return _OV_Tv_1; } set { _OV_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM OV_Tv_2 { get { return _OV_Tv_2; } set { _OV_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM OV_Tv_3 { get { return _OV_Tv_3; } set { _OV_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM OV_Rv_1 { get { return _OV_Rv_1; } set { _OV_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM OV_Rv_2 { get { return _OV_Rv_2; } set { _OV_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM OV_Rv_3 { get { return _OV_Rv_3; } set { _OV_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM OV_Dt_1 { get { return _OV_Dt_1; } set { _OV_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM OV_Dt_2 { get { return _OV_Dt_2; } set { _OV_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM OV_Dt_3 { get { return _OV_Dt_3; } set { _OV_Dt_3 = value; OnPropertyChanged(); } }
        public EEPROM UV_Tv_1 { get { return _UV_Tv_1; } set { _UV_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM UV_Tv_2 { get { return _UV_Tv_2; } set { _UV_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM UV_Tv_3 { get { return _UV_Tv_3; } set { _UV_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM UV_Rv_1 { get { return _UV_Rv_1; } set { _UV_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM UV_Rv_2 { get { return _UV_Rv_2; } set { _UV_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM UV_Rv_3 { get { return _UV_Rv_3; } set { _UV_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM UV_Dt_1 { get { return _UV_Dt_1; } set { _UV_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM UV_Dt_2 { get { return _UV_Dt_2; } set { _UV_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM UV_Dt_3 { get { return _UV_Dt_3; } set { _UV_Dt_3 = value; OnPropertyChanged(); } }

        public EEPROM OC_C_Tv_1 { get { return _OC_C_Tv_1; } set { _OC_C_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Tv_2 { get { return _OC_C_Tv_2; } set { _OC_C_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Tv_3 { get { return _OC_C_Tv_3; } set { _OC_C_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Rt_1 { get { return _OC_C_Rt_1; } set { _OC_C_Rt_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Rt_2 { get { return _OC_C_Rt_2; } set { _OC_C_Rt_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Rt_3 { get { return _OC_C_Rt_3; } set { _OC_C_Rt_3 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Dt_1 { get { return _OC_C_Dt_1; } set { _OC_C_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Dt_2 { get { return _OC_C_Dt_2; } set { _OC_C_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Dt_3 { get { return _OC_C_Dt_3; } set { _OC_C_Dt_3 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Tv_1 { get { return _OC_D_Tv_1; } set { _OC_D_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Tv_2 { get { return _OC_D_Tv_2; } set { _OC_D_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Tv_3 { get { return _OC_D_Tv_3; } set { _OC_D_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Rt_1 { get { return _OC_D_Rt_1; } set { _OC_D_Rt_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Rt_2 { get { return _OC_D_Rt_2; } set { _OC_D_Rt_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Rt_3 { get { return _OC_D_Rt_3; } set { _OC_D_Rt_3 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Dt_1 { get { return _OC_D_Dt_1; } set { _OC_D_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Dt_2 { get { return _OC_D_Dt_2; } set { _OC_D_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Dt_3 { get { return _OC_D_Dt_3; } set { _OC_D_Dt_3 = value; OnPropertyChanged(); } }

        public EEPROM OT_C_Tv_1 { get { return _OT_C_Tv_1; } set { _OT_C_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Tv_2 { get { return _OT_C_Tv_2; } set { _OT_C_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Tv_3 { get { return _OT_C_Tv_3; } set { _OT_C_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Rv_1 { get { return _OT_C_Rv_1; } set { _OT_C_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Rv_2 { get { return _OT_C_Rv_2; } set { _OT_C_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Rv_3 { get { return _OT_C_Rv_3; } set { _OT_C_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Dt_1 { get { return _OT_C_Dt_1; } set { _OT_C_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Dt_2 { get { return _OT_C_Dt_2; } set { _OT_C_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_C_Dt_3 { get { return _OT_C_Dt_3; } set { _OT_C_Dt_3 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Tv_1 { get { return _OT_D_Tv_1; } set { _OT_D_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Tv_2 { get { return _OT_D_Tv_2; } set { _OT_D_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Tv_3 { get { return _OT_D_Tv_3; } set { _OT_D_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Rv_1 { get { return _OT_D_Rv_1; } set { _OT_D_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Rv_2 { get { return _OT_D_Rv_2; } set { _OT_D_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Rv_3 { get { return _OT_D_Rv_3; } set { _OT_D_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Dt_1 { get { return _OT_D_Dt_1; } set { _OT_D_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Dt_2 { get { return _OT_D_Dt_2; } set { _OT_D_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM OT_D_Dt_3 { get { return _OT_D_Dt_3; } set { _OT_D_Dt_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Tv_1 { get { return _UT_C_Tv_1; } set { _UT_C_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Tv_2 { get { return _UT_C_Tv_2; } set { _UT_C_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Tv_3 { get { return _UT_C_Tv_3; } set { _UT_C_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Rv_1 { get { return _UT_C_Rv_1; } set { _UT_C_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Rv_2 { get { return _UT_C_Rv_2; } set { _UT_C_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Rv_3 { get { return _UT_C_Rv_3; } set { _UT_C_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Dt_1 { get { return _UT_C_Dt_1; } set { _UT_C_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Dt_2 { get { return _UT_C_Dt_2; } set { _UT_C_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_C_Dt_3 { get { return _UT_C_Dt_3; } set { _UT_C_Dt_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Tv_1 { get { return _UT_D_Tv_1; } set { _UT_D_Tv_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Tv_2 { get { return _UT_D_Tv_2; } set { _UT_D_Tv_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Tv_3 { get { return _UT_D_Tv_3; } set { _UT_D_Tv_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Rv_1 { get { return _UT_D_Rv_1; } set { _UT_D_Rv_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Rv_2 { get { return _UT_D_Rv_2; } set { _UT_D_Rv_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Rv_3 { get { return _UT_D_Rv_3; } set { _UT_D_Rv_3 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Dt_1 { get { return _UT_D_Dt_1; } set { _UT_D_Dt_1 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Dt_2 { get { return _UT_D_Dt_2; } set { _UT_D_Dt_2 = value; OnPropertyChanged(); } }
        public EEPROM UT_D_Dt_3 { get { return _UT_D_Dt_3; } set { _UT_D_Dt_3 = value; OnPropertyChanged(); } }

        public EEPROM SC_Tv { get { return _SC_Tv; } set { _SC_Tv = value; OnPropertyChanged(); } }
        public EEPROM SC_Rt { get { return _SC_Rt; } set { _SC_Rt = value; OnPropertyChanged(); } }
        public EEPROM SC_Dt { get { return _SC_Dt; } set { _SC_Dt = value; OnPropertyChanged(); } }
        public EEPROM HV_Dt { get { return _HV_Dt; } set { _HV_Dt = value; OnPropertyChanged(); } }
        public EEPROM LV_Dt { get { return _LV_Dt; } set { _LV_Dt = value; OnPropertyChanged(); } }
        public EEPROM HV_Cutoff { get { return _HV_Cutoff; } set { _HV_Cutoff = value; OnPropertyChanged(); } }
        public EEPROM LV_Cutoff { get { return _LV_Cutoff; } set { _LV_Cutoff = value; OnPropertyChanged(); } }
        public EEPROM Temp_UpperLimit { get { return _Temp_UpperLimit; } set { _Temp_UpperLimit = value; OnPropertyChanged(); } }
        public EEPROM Temp_LowerLimit { get { return _Temp_LowerLimit; } set { _Temp_LowerLimit = value; OnPropertyChanged(); } }

        public EEPROM Charging_Voltage { get { return _Charging_Voltage; } set { _Charging_Voltage = value; OnPropertyChanged(); } }
        public EEPROM Charging_Current { get { return _Charging_Current; } set { _Charging_Current = value; OnPropertyChanged(); } }
        public EEPROM FC_Tv { get { return _FC_Tv; } set { _FC_Tv = value; OnPropertyChanged(); } }
        public EEPROM FC_Rv { get { return _FC_Rv; } set { _FC_Rv = value; OnPropertyChanged(); } }
        public EEPROM FC_Dt { get { return _FC_Dt; } set { _FC_Dt = value; OnPropertyChanged(); } }
        public EEPROM FD_Tv { get { return _FD_Tv; } set { _FD_Tv = value; OnPropertyChanged(); } }
        public EEPROM FD_Rv { get { return _FD_Rv; } set { _FD_Rv = value; OnPropertyChanged(); } }
        public EEPROM FD_Dt { get { return _FD_Dt; } set { _FD_Dt = value; OnPropertyChanged(); } }
        public EEPROM Charge_State { get { return _Charge_State; } set { _Charge_State = value; OnPropertyChanged(); } }
        public EEPROM Charge_TimeOut { get { return _Charge_TimeOut; } set { _Charge_TimeOut = value; OnPropertyChanged(); } }

        public EEPROM Power_Consumption { get { return _Power_Consumption; } set { _Power_Consumption = value; OnPropertyChanged(); } }
        public EEPROM Cyclic_Capacity { get { return _Cyclic_Capacity; } set { _Cyclic_Capacity = value; OnPropertyChanged(); } }
        public EEPROM Cumulative_Capability { get { return _Cumulative_Capability; } set { _Cumulative_Capability = value; OnPropertyChanged(); } }
        public EEPROM Storage_Capability { get { return _Storage_Capability; } set { _Storage_Capability = value; OnPropertyChanged(); } }
        public EEPROM Available_Capacity { get { return _Available_Capacity; } set { _Available_Capacity = value; OnPropertyChanged(); } }

        public EEPROM Gain_1 { get { return _Gain_1; } set { _Gain_1 = value; OnPropertyChanged(); } }
        public EEPROM Offset_1 { get { return _Offset_1; } set { _Offset_1 = value; OnPropertyChanged(); } }
        public EEPROM Shunt_1 { get { return _Shunt_1; } set { _Shunt_1 = value; OnPropertyChanged(); } }
        public EEPROM Reverse_Currrent { get { return _Reverse_Currrent; } set { _Reverse_Currrent = value; OnPropertyChanged(); } }
        public EEPROM Gain_2 { get { return _Gain_2; } set { _Gain_2 = value; OnPropertyChanged(); } }
        public EEPROM Offset_2 { get { return _Offset_2; } set { _Offset_2 = value; OnPropertyChanged(); } }
        public EEPROM Shunt_2 { get { return _Shunt_2; } set { _Shunt_2 = value; OnPropertyChanged(); } }
        public EEPROM Ignored_Current { get { return _Ignored_Current; } set { _Ignored_Current = value; OnPropertyChanged(); } }

        public EEPROM OV_Count { get { return _OV_Count; } set { _OV_Count = value; OnPropertyChanged(); } }
        public EEPROM UV_Count { get { return _UV_Count; } set { _UV_Count = value; OnPropertyChanged(); } }
        public EEPROM OT_Count { get { return _OT_Count; } set { _OT_Count = value; OnPropertyChanged(); } }
        public EEPROM UT_Count { get { return _UT_Count; } set { _UT_Count = value; OnPropertyChanged(); } }
        public EEPROM OC_C_Count { get { return _OC_C_Count; } set { _OC_C_Count = value; OnPropertyChanged(); } }
        public EEPROM OC_D_Count { get { return _OC_D_Count; } set { _OC_D_Count = value; OnPropertyChanged(); } }
        public EEPROM SC_Count { get { return _SC_Count; } set { _SC_Count = value; OnPropertyChanged(); } }
        public EEPROM Boot { get { return _Boot; } set { _Boot = value; OnPropertyChanged(); } }

        public EEPROM CellV_Max { get { return _CellV_Max; } set { _CellV_Max = value; OnPropertyChanged(); } }
        public EEPROM CellV_Min { get { return _CellV_Min; } set { _CellV_Min = value; OnPropertyChanged(); } }
        public EEPROM CellT_Max { get { return _CellT_Max; } set { _CellT_Max = value; OnPropertyChanged(); } }
        public EEPROM CellT_Min { get { return _CellT_Min; } set { _CellT_Min = value; OnPropertyChanged(); } }
        public EEPROM CHG_Cur_Max { get { return _CHG_Cur_Max; } set { _CHG_Cur_Max = value; OnPropertyChanged(); } }
        public EEPROM DSG_Cur_Max { get { return _DSG_Cur_Max; } set { _DSG_Cur_Max = value; OnPropertyChanged(); } }
        public EEPROM SOC_EE { get { return _SOC_EE; } set { _SOC_EE = value; OnPropertyChanged(); } }
        public EEPROM SOH_EE { get { return _SOH_EE; } set { _SOH_EE = value; OnPropertyChanged(); } }
        public EEPROM Charge_Cycle { get { return _Charge_Cycle; } set { _Charge_Cycle = value; OnPropertyChanged(); } }

        private ObservableCollection<EEPROM> obs_EE = new ObservableCollection<EEPROM>();
        public ObservableCollection<EEPROM> OBS_EE { get { return obs_EE; } set { obs_EE = value; OnPropertyChanged(); } }
        
        void EEini()
        {
            OBS_EE.Add(Rated_Capacity);
            OBS_EE.Add(Nominal_Voltage);
            OBS_EE.Add(Packs_EE);
            OBS_EE.Add(Cells_EE);
            OBS_EE.Add(Temps_EE);
            OBS_EE.Add(Current_Model);
            OBS_EE.Add(Firmware_Version);

            OBS_EE.Add(ID_L);
            OBS_EE.Add(ID_H);

            OBS_EE.Add(OV_Tv_1);
            OBS_EE.Add(OV_Tv_2);
            OBS_EE.Add(OV_Tv_3);
            OBS_EE.Add(OV_Rv_1);
            OBS_EE.Add(OV_Rv_2);
            OBS_EE.Add(OV_Rv_3);
            OBS_EE.Add(OV_Dt_1);
            OBS_EE.Add(OV_Dt_2);
            OBS_EE.Add(OV_Dt_3);
            OBS_EE.Add(UV_Tv_1);
            OBS_EE.Add(UV_Tv_2);
            OBS_EE.Add(UV_Tv_3);
            OBS_EE.Add(UV_Rv_1);
            OBS_EE.Add(UV_Rv_2);
            OBS_EE.Add(UV_Rv_3);
            OBS_EE.Add(UV_Dt_1);
            OBS_EE.Add(UV_Dt_2);
            OBS_EE.Add(UV_Dt_3);

            OBS_EE.Add(OC_C_Tv_1);
            OBS_EE.Add(OC_C_Tv_2);
            OBS_EE.Add(OC_C_Tv_3);
            OBS_EE.Add(OC_C_Rt_1);
            OBS_EE.Add(OC_C_Rt_2);
            OBS_EE.Add(OC_C_Rt_3);
            OBS_EE.Add(OC_C_Dt_1);
            OBS_EE.Add(OC_C_Dt_2);
            OBS_EE.Add(OC_C_Dt_3);
            OBS_EE.Add(OC_D_Tv_1);
            OBS_EE.Add(OC_D_Tv_2);
            OBS_EE.Add(OC_D_Tv_3);
            OBS_EE.Add(OC_D_Rt_1);
            OBS_EE.Add(OC_D_Rt_2);
            OBS_EE.Add(OC_D_Rt_3);
            OBS_EE.Add(OC_D_Dt_1);
            OBS_EE.Add(OC_D_Dt_2);
            OBS_EE.Add(OC_D_Dt_3);

            OBS_EE.Add(OT_C_Tv_1);
            OBS_EE.Add(OT_C_Tv_2);
            OBS_EE.Add(OT_C_Tv_3);
            OBS_EE.Add(OT_C_Rv_1);
            OBS_EE.Add(OT_C_Rv_2);
            OBS_EE.Add(OT_C_Rv_3);
            OBS_EE.Add(OT_C_Dt_1);
            OBS_EE.Add(OT_C_Dt_2);
            OBS_EE.Add(OT_C_Dt_3);
            OBS_EE.Add(OT_D_Tv_1);
            OBS_EE.Add(OT_D_Tv_2);
            OBS_EE.Add(OT_D_Tv_3);
            OBS_EE.Add(OT_D_Rv_1);
            OBS_EE.Add(OT_D_Rv_2);
            OBS_EE.Add(OT_D_Rv_3);
            OBS_EE.Add(OT_D_Dt_1);
            OBS_EE.Add(OT_D_Dt_2); 
            OBS_EE.Add(OT_D_Dt_3); 
            OBS_EE.Add(UT_C_Tv_1); 
            OBS_EE.Add(UT_C_Tv_2); 
            OBS_EE.Add(UT_C_Tv_3); 
            OBS_EE.Add(UT_C_Rv_1); 
            OBS_EE.Add(UT_C_Rv_2); 
            OBS_EE.Add(UT_C_Rv_3); 
            OBS_EE.Add(UT_C_Dt_1); 
            OBS_EE.Add(UT_C_Dt_2);
            OBS_EE.Add(UT_C_Dt_3); 
            OBS_EE.Add(UT_D_Tv_1); 
            OBS_EE.Add(UT_D_Tv_2); 
            OBS_EE.Add(UT_D_Tv_3); 
            OBS_EE.Add(UT_D_Rv_1);
            OBS_EE.Add(UT_D_Rv_2); 
            OBS_EE.Add(UT_D_Rv_3); 
            OBS_EE.Add(UT_D_Dt_1); 
            OBS_EE.Add(UT_D_Dt_2); 
            OBS_EE.Add(UT_D_Dt_3);

            OBS_EE.Add(SC_Tv);
            OBS_EE.Add(SC_Rt); 
            OBS_EE.Add(SC_Dt); 
            OBS_EE.Add(HV_Dt);
            OBS_EE.Add(LV_Dt);
            OBS_EE.Add(HV_Cutoff); 
            OBS_EE.Add(LV_Cutoff); 
            OBS_EE.Add(Temp_UpperLimit); 
            OBS_EE.Add(Temp_LowerLimit); 

            OBS_EE.Add(Charging_Voltage); 
            OBS_EE.Add(Charging_Current);
            OBS_EE.Add(FC_Tv); 
            OBS_EE.Add(FC_Rv); 
            OBS_EE.Add(FC_Dt); 
            OBS_EE.Add(FD_Tv); 
            OBS_EE.Add(FD_Rv); 
            OBS_EE.Add(FD_Dt); 
            OBS_EE.Add(Charge_State); 

            OBS_EE.Add(Power_Consumption);
            OBS_EE.Add(SOC_EE); 
            OBS_EE.Add(SOH_EE); 
            OBS_EE.Add(Cumulative_Capability);
            OBS_EE.Add(Storage_Capability); 
            OBS_EE.Add(Available_Capacity); 

            OBS_EE.Add(Gain_1); 
            OBS_EE.Add(Offset_1); 
            OBS_EE.Add(Shunt_1);
            OBS_EE.Add(Reverse_Currrent);
            OBS_EE.Add(Gain_2); 
            OBS_EE.Add(Offset_2); 
            OBS_EE.Add(Shunt_2); 
            OBS_EE.Add(Ignored_Current); 

            OBS_EE.Add(OV_Count); 
            OBS_EE.Add(UV_Count); 
            OBS_EE.Add(OT_Count); 
            OBS_EE.Add(UT_Count); 
            OBS_EE.Add(OC_C_Count); 
            OBS_EE.Add(OC_D_Count); 
            OBS_EE.Add(SC_Count);
            OBS_EE.Add(Boot); 

            OBS_EE.Add(CellV_Max); 
            OBS_EE.Add(CellV_Min); 
            OBS_EE.Add(CellT_Max); 
            OBS_EE.Add(CellT_Min); 
            OBS_EE.Add(CHG_Cur_Max);
            OBS_EE.Add(DSG_Cur_Max);
            OBS_EE.Add(Cyclic_Capacity); 
            OBS_EE.Add(Charge_Cycle); 
        }
        #endregion

        #region UI
        private ObservableCollection<String> obs_CAN_SPEED = new ObservableCollection<String>();
        public ObservableCollection<String> OBS_CAN_SPEED { get { return obs_CAN_SPEED; } set { obs_CAN_SPEED = value; OnPropertyChanged(); } }
        private ObservableCollection<int> obs_TEMP = new ObservableCollection<int>();
        public ObservableCollection<int> OBS_TEMP { get { return obs_TEMP; } set { obs_TEMP = value; OnPropertyChanged(); } }
        private ObservableCollection<int> obs_CELL = new ObservableCollection<int>();
        public ObservableCollection<int> OBS_CELL { get { return obs_CELL; } set { obs_CELL = value; OnPropertyChanged(); } }

        public static double voltage_outlier = 123;
        public double Voltage_outlier { get { return voltage_outlier; } set { voltage_outlier = value; OnPropertyChanged(); } }
        public static double voltage_high = 4500;
        public double Voltage_high { get { return voltage_high; } set { voltage_high = value; OnPropertyChanged(); } }
        public static double voltage_low = 2800;
        public double Voltage_low { get { return voltage_low; } set { voltage_low = value; OnPropertyChanged(); } }
        public static double temperature_high = 50;
        public double Temperature_high { get { return temperature_high; } set { temperature_high = value; OnPropertyChanged(); } }
        public static double temperature_low = 20;
        public double Temperature_low { get { return temperature_low; } set { temperature_low = value; OnPropertyChanged(); } }
        public static UInt16 _Log_Interval = 1;
        public UInt16 Log_Interval { get { return _Log_Interval; } set { _Log_Interval = value;OnPropertyChanged(); } }

        private String _FirmwareView = "NULL";
        public String FirmwareView { get { return _FirmwareView; } set { _FirmwareView = value; OnPropertyChanged(); } }
        private String _IDView = "NULL";
        public String IDView { get { return _IDView; } set { _IDView = value; OnPropertyChanged(); } }
        private bool _Light = false;
        public bool Light { get { return _Light; } set { _Light = value;OnPropertyChanged(); } }
        private bool _CONNECT = false;
        public bool CONNECT { get { return _CONNECT; } set { _CONNECT = value;OnPropertyChanged(); } }
        private bool _LOG = false;
        public bool LOG { get { return _LOG; } set { _LOG = value;OnPropertyChanged(); } }

        private String _Passwrod = String.Empty;
        public String Password { get { return _Passwrod; } set { _Passwrod = value;OnPropertyChanged(); } }

        private byte[] _CalibrationCur = new byte[2] { 0, 0 };
        public byte[] CalibrationCur { get { return _CalibrationCur; } set { _CalibrationCur = value;OnPropertyChanged(); } }

        void UI()
        {
            OBS_CAN_SPEED.Add("250K");
            OBS_CAN_SPEED.Add("500K");
            OBS_CAN_SPEED.Add("1000K");

            for(int i = 1; i < 13; i++)
            {
                OBS_CELL.Add(i);
                OBS_TEMP.Add(i);
            }
        }
        #endregion
        public MainWindowVM()
        {
            CELL = Convert.ToInt32(ConfigurationManager.AppSettings["CELL"]); Console.WriteLine("CELL: " + CELL);
            PACK = Convert.ToInt32(ConfigurationManager.AppSettings["PACK"]); Console.WriteLine("PACK: " + PACK);
            TEMP = Convert.ToInt32(ConfigurationManager.AppSettings["TEMP"]); Console.WriteLine("TEMP: " + TEMP);
            CAN_SPEED = ConfigurationManager.AppSettings["CAN_SPEED"]; Console.WriteLine("CAN_SPEED: " + CAN_SPEED);
            LOG_PATH = ConfigurationManager.AppSettings["LOG_PATH"]; Console.WriteLine("LOG_PATH: " + LOG_PATH);
            Voltage_outlier = Convert.ToDouble(ConfigurationManager.AppSettings["Voltage_outlier"]); Console.WriteLine("Voltage_outlier: " + Voltage_outlier);
            Voltage_high = Convert.ToDouble(ConfigurationManager.AppSettings["Voltage_high"]); Console.WriteLine("Voltage_high: " + Voltage_high);
            Voltage_low = Convert.ToDouble(ConfigurationManager.AppSettings["Voltage_low"]); Console.WriteLine("Voltage_low: " + Voltage_low);
            Temperature_high = Convert.ToDouble(ConfigurationManager.AppSettings["Temperature_high"]); Console.WriteLine("Temperature_high: " + Temperature_high);
            Temperature_low = Convert.ToDouble(ConfigurationManager.AppSettings["Temperature_low"]); Console.WriteLine("Temperature_low: " + Temperature_low);
            Log_Interval = Convert.ToUInt16(ConfigurationManager.AppSettings["Log_Interval"]); Console.WriteLine("Log_Interval: " + Log_Interval);

            UI();
            EEini();

            Package = new Package[PACK];
            for (int i = 0; i < PACK; i++)
            {
                Package[i] = new Package(i + 1);
                OBS.Add(Package[i]);
            }

            TimerStart();

            //test
            //ETHERNET();

            Task.Run(() =>
            {
                while (true)
                {
                    if (CONNECT) CanProtocal(Receive());
                }
            });

            //First Read EE
            Task.Run(() =>
            {
                System.Threading.Thread.Sleep(2000);
                EE_Read();
            });
        }

        #region Ethernet
        System.Net.Sockets.Socket socket = new System.Net.Sockets.Socket(System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
        
        void ETHERNET()
        {
            //socket.Connect("127.0.0.1", 2502);
            //CONNECT = true;
            byte[] buffer = new byte[1024];
            Task.Run(() =>
            {
                while (true)
                {
                    if (CONNECT)            
                    {
                        try
                        {
                            try
                            {
                                Console.WriteLine(socket.Send(new byte[1] { 0 }));
                            }
                            catch(Exception eee)
                            {
                                Console.WriteLine("SEND FAIL");
                            }
                            Light = !Light;
                            int pt = socket.Receive(buffer, buffer.Length, System.Net.Sockets.SocketFlags.None);//WiFi Bug
                            Console.WriteLine("Length: " + pt);
                            foreach (byte b in buffer)
                                Console.Write(b + "-");
                            Console.WriteLine();
                            EthernetProtocal(buffer);
                            Array.Clear(buffer, 0, buffer.Length);

                        }
                        catch (Exception ee)
                        {
                            Console.WriteLine("Read Fail!");
                            CONNECT = false;
                            timer_reconnect.Start();
                        }
                    }
                }
            });
            
        }

        void EthernetProtocal(byte[] input)
        {
            if (input.Length < 200)
            {
                Console.WriteLine("Data Error!");
                return;
            }

            if(input[0] == 0x01)
            {
                Battery.Voltage = ((UInt16)(input[1] + (input[2] << 8)) / 100.0);
                Battery.Current = ((Int16)(input[3] + (input[4] << 8)) / 100.0);
                Battery.Temperature = (Int16)(input[5] + (input[6] << 8));
                Battery.Soc = input[7];
                Battery.Errorcode = input[16];
                Battery.Status = (UInt16)(input[31] + (input[32] << 8));
                Battery.Relay = input[33];
            }
            else if(input[0] == 0x02)
            {
                for(int i = 0; i < PACK; i++)
                {
                    Package[i].Voltage.Cell_1 = (input[i * 22 + 1] + (input[i * 22 + 2] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_2 = (input[i * 22 + 3] + (input[i * 22 + 4] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_3 = (input[i * 22 + 5] + (input[i * 22 + 6] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_4 = (input[i * 22 + 7] + (input[i * 22 + 8] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_5 = (input[i * 22 + 9] + (input[i * 22 + 10] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_6 = (input[i * 22 + 11] + (input[i * 22 + 12] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_7 = (input[i * 22 + 13] + (input[i * 22 + 14] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_8 = (input[i * 22 + 15] + (input[i * 22 + 16] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_9 = (input[i * 22 + 17] + (input[i * 22 + 18] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_10 = (input[i * 22 + 19] + (input[i * 22 + 20] << 8)) / 10000.0;
                    Package[i].Voltage.Cell_11 = (input[i * 22 + 21] + (input[i * 22 + 22] << 8)) / 10000.0;

                    double tV = 0, tVmax = double.MinValue, tVmin = double.MaxValue;
                    for (int j = 0; j < CELL; j++)
                    {
                        tV += Package[i].Voltage.cell[j];
                        tVmax = tVmax > Package[i].Voltage.cell[j] ? tVmax : Package[i].Voltage.cell[j];
                        tVmin = tVmin < Package[i].Voltage.cell[j] ? tVmin : Package[i].Voltage.cell[j];
                    }
                    Package[i].Voltage.Total = tV;   //cell總電壓
                    Package[i].Voltage.Max = tVmax;  //cell最大電壓
                    Package[i].Voltage.Min = tVmin;  //cell最小電壓
                    Package[i].Voltage.Dif = tVmax - tVmin;  //cell電壓差

                    if (tVmax - tVmin > Voltage_outlier)    //Dif__Flag
                        Package[i].Voltage.Dif_flag = true;
                }

            }
            else if(input[0] == 0x04)
            {
                for (int i = 0; i < PACK; i++)
                {
                    Package[i].Temperature.Cell_1 = (Int16)(input[i * 24 + 1] + (input[i * 24 + 2] << 8)) ;
                    Package[i].Temperature.Cell_2 = (Int16)(input[i * 24 + 3] + (input[i * 24 + 4] << 8)) ;
                    Package[i].Temperature.Cell_3 = (Int16)(input[i * 24 + 5] + (input[i * 24 + 6] << 8)) ;
                    Package[i].Temperature.Cell_4 = (Int16)(input[i * 24 + 7] + (input[i * 24 + 8] << 8)) ;
                    Package[i].Temperature.Cell_5 = (Int16)(input[i * 24 + 9] + (input[i * 24 + 10] << 8)) ;
                    Package[i].Temperature.Cell_6 = (Int16)(input[i * 24 + 11] + (input[i * 24 + 12] << 8)) ;
                    Package[i].Temperature.Cell_7 = (Int16)(input[i * 24 + 13] + (input[i * 24 + 14] << 8)) ;
                    Package[i].Temperature.Cell_8 = (Int16)(input[i * 24 + 15] + (input[i * 24 + 16] << 8)) ;
                    Package[i].Temperature.Cell_9 = (Int16)(input[i * 24 + 17] + (input[i * 24 + 18] << 8)) ;
                    Package[i].Temperature.Cell_10 = (Int16)(input[i * 24 + 19] + (input[i * 24 + 20] << 8)) ;
                    Package[i].Temperature.Cell_11 = (Int16)(input[i * 24 + 21] + (input[i * 24 + 22] << 8)) ;
                    Package[i].Temperature.Cell_12 = (Int16)(input[i * 24 + 23] + (input[i * 24 + 24] << 8)) ;

                    double tT = 0, tTmax = double.MinValue, tTmin = double.MaxValue;
                    for (int j = 0; j < TEMP; j++)
                    {
                        tT += Package[i].Temperature.cell[j];
                        tTmax = tTmax > Package[i].Temperature.cell[j] ? tTmax : Package[i].Temperature.cell[j];
                        tTmin = tTmin < Package[i].Temperature.cell[j] ? tTmin : Package[i].Temperature.cell[j];
                    }
                    Package[i].Temperature.Average = tT / TEMP;   //cell平均溫度
                    Package[i].Temperature.Max = tTmax;  //cell最高溫度
                    Package[i].Temperature.Min = tTmin;  //cell最低溫度
                    Package[i].Temperature.Dif = tTmax - tTmin;  //cell溫度差
                }
            }
        }
        #endregion

        //2020-04-06 Format
        private void CanProtocal(List<String> datas)
        {
            if (datas == null) return;

            foreach (var data in datas)
            {
                string[] t = data.Split('-');
                string ID = t[0];
                string[] DATA = new string[8];
                for (int i = 0; i < DATA.Length; i++)
                    DATA[i] = t[1].Substring(i * 2, 2);

                //資料分類
                if (ID == "1800A7F4")
                {
                    if (DATA[0] == "01")
                    {
                        Light = !Light;//燈號Flag
                        Battery.Voltage = ((UInt16)Hex2int(DATA[1] + DATA[2]) / 100.0);
                        Battery.Temperature = ((Int16)Hex2int(DATA[3] + DATA[4]) / 100.0);
                        Battery.Current = ((Int16)Hex2int(DATA[5] + DATA[6]) / 100.0);
                        Battery.Soc = byte.Parse(DATA[7], NumberStyles.HexNumber);
                    }
                    else if (DATA[0] == "02")
                    {
                        Battery.Status = (UInt16)Hex2int(DATA[1] + DATA[2]);
                        Battery.Errorcode = (Byte)Hex2int(DATA[3]);
                        Battery.Relay = (Byte)Hex2int(DATA[4]);
                    }
                    else if (DATA[0] == "03")
                    {
                        int tt = int.Parse(DATA[4], NumberStyles.HexNumber) + (int.Parse(DATA[5], NumberStyles.HexNumber) << 8);
                        Battery.BMSVersion = (UInt16)Hex2int(DATA[1] + DATA[2]);
                        Battery.FIRMWAREVersion = (UInt16)Hex2int(DATA[3] + DATA[4]);
                        //((tt >> 11) & 0b11111) * 100000 + ((tt >> 7) & 0b1111) * 1000 + ((tt >> 2) & 0b11111) * 10 + (tt & 0b11);
                        //電流版暫時不用
                        //tt = int.Parse(DATA[6], NumberStyles.HexNumber) + (int.Parse(DATA[7], NumberStyles.HexNumber) << 8);
                        //EEPROMViewModel.FirmwareVersion_CUR.Value = ((tt >> 11) & 0b11111) * 100000 + ((tt >> 7) & 0b1111) * 1000 + ((tt >> 2) & 0b11111) * 10 + (tt & 0b11);
                    }

                }
                else if (ID == "1801A7F4")
                {
                    int PackNum = Hex2int(DATA[0]) / 16 - 1; // -1 0開始
                    if (PackNum < 0) break;//防錯誤

                    if (DATA[0][1] == '0')
                    {
                        Package[PackNum].Voltage.Cell_1 = Hex2int(DATA[1] + DATA[2]) / 10000.0;
                        Package[PackNum].Voltage.Cell_2 = Hex2int(DATA[3] + DATA[4]) / 10000.0;
                        Package[PackNum].Voltage.Cell_3 = Hex2int(DATA[5] + DATA[6]) / 10000.0;
                    }
                    else if (DATA[0][1] == '1')
                    {
                        Package[PackNum].Voltage.Cell_4 = Hex2int(DATA[1] + DATA[2]) / 10000.0;
                        Package[PackNum].Voltage.Cell_5 = Hex2int(DATA[3] + DATA[4]) / 10000.0;
                        Package[PackNum].Voltage.Cell_6 = Hex2int(DATA[5] + DATA[6]) / 10000.0;
                    }
                    else if (DATA[0][1] == '2')
                    {
                        Package[PackNum].Voltage.Cell_7 = Hex2int(DATA[1] + DATA[2]) / 10000.0;
                        Package[PackNum].Voltage.Cell_8 = Hex2int(DATA[3] + DATA[4]) / 10000.0;
                        Package[PackNum].Voltage.Cell_9 = Hex2int(DATA[5] + DATA[6]) / 10000.0;
                    }
                    else if (DATA[0][1] == '3')
                    {
                        Package[PackNum].Voltage.Cell_10 = Hex2int(DATA[1] + DATA[2]) / 10000.0;
                        Package[PackNum].Voltage.Cell_11 = Hex2int(DATA[3] + DATA[4]) / 10000.0;
                        Package[PackNum].Voltage.Cell_12 = Hex2int(DATA[5] + DATA[6]) / 10000.0;
                    }
                    else if (DATA[0][1] == '8')
                    {
                        Package[PackNum].Temperature.Cell_1 = (Int16)Hex2int(DATA[1] + DATA[2]) / 100.0;
                        Package[PackNum].Temperature.Cell_2 = (Int16)Hex2int(DATA[3] + DATA[4]) / 100.0;
                        Package[PackNum].Temperature.Cell_3 = (Int16)Hex2int(DATA[5] + DATA[6]) / 100.0;
                    }
                    else if (DATA[0][1] == '9')
                    {
                        Package[PackNum].Temperature.Cell_4 = (Int16)Hex2int(DATA[1] + DATA[2]) / 100.0;
                        Package[PackNum].Temperature.Cell_5 = (Int16)Hex2int(DATA[3] + DATA[4]) / 100.0;
                        Package[PackNum].Temperature.Cell_6 = (Int16)Hex2int(DATA[5] + DATA[6]) / 100.0;
                    }
                    else if (DATA[0][1] == 'A' || DATA[0][1] == 'a')
                    {
                        Package[PackNum].Temperature.Cell_7 = (Int16)Hex2int(DATA[1] + DATA[2]) / 100.0;
                        Package[PackNum].Temperature.Cell_8 = (Int16)Hex2int(DATA[3] + DATA[4]) / 100.0;
                        Package[PackNum].Temperature.Cell_9 = (Int16)Hex2int(DATA[5] + DATA[6]) / 100.0;
                    }
                    else if (DATA[0][1] == 'B' || DATA[0][1] == 'b')
                    {
                        Package[PackNum].Temperature.Cell_10 = (Int16)Hex2int(DATA[1] + DATA[2]) / 100.0;
                        Package[PackNum].Temperature.Cell_11 = (Int16)Hex2int(DATA[3] + DATA[4]) / 100.0;
                        Package[PackNum].Temperature.Cell_12 = (Int16)Hex2int(DATA[5] + DATA[6]) / 100.0;
                    }

                    //未來需優化，每筆皆重新算不優!
                    //MVVM破壞，直接使用MODEL FIELD
                    double tV = 0, tVmax = double.MinValue, tVmin = double.MaxValue;
                    double tT = 0, tTmax = double.MinValue, tTmin = double.MaxValue;
                    for (int i = 0; i < CELL; i++)
                    {
                        tV += Package[PackNum].Voltage.cell[i];
                        tVmax = tVmax > Package[PackNum].Voltage.cell[i] ? tVmax : Package[PackNum].Voltage.cell[i];
                        tVmin = tVmin < Package[PackNum].Voltage.cell[i] ? tVmin : Package[PackNum].Voltage.cell[i];
                    }
                    for (int i = 0; i < TEMP; i++)
                    {
                        tT += Package[PackNum].Temperature.cell[i];
                        tTmax = tTmax > Package[PackNum].Temperature.cell[i] ? tTmax : Package[PackNum].Temperature.cell[i];
                        tTmin = tTmin < Package[PackNum].Temperature.cell[i] ? tTmin : Package[PackNum].Temperature.cell[i];
                    }
                    Package[PackNum].Voltage.Total = tV;   //cell總電壓
                    Package[PackNum].Voltage.Max = tVmax;  //cell最大電壓
                    Package[PackNum].Voltage.Min = tVmin;  //cell最小電壓
                    Package[PackNum].Voltage.Dif = tVmax - tVmin;  //cell電壓差
                    Package[PackNum].Temperature.Average = tT / TEMP;   //cell平均溫度
                    Package[PackNum].Temperature.Max = tTmax;  //cell最高溫度
                    Package[PackNum].Temperature.Min = tTmin;  //cell最低溫度
                    Package[PackNum].Temperature.Dif = tTmax - tTmin;  //cell溫度差

                    if (tVmax - tVmin > (Voltage_outlier / 1000.0))    //Dif__Flag
                        Package[PackNum].Voltage.Dif_flag = true;
                }

                if (ID.Substring(4, 4) == "F0EE")      //EEPROM
                {
                    //新版EE 堪用
                    byte address = (Byte)Hex2int(ID.Substring(2, 2));
                    int skip = 0;
                    byte[] dt = new byte[8];//轉成byte[]
                    for (int i = 0; i < 8; i++)
                        dt[i] = (byte)Hex2int(DATA[i]);
                    foreach (EEPROM ee in OBS_EE) 
                    {
                        if(ee.Index == address)
                        {
                            //Console.WriteLine("Address:" + address);
                            ee.Data = dt.Skip(skip).Take(ee.Len).ToArray();
                            address += ee.Len;
                            if (address % 8 == 0) break;

                            ee.Change = false;
                        }
                    }

                    //ID & Version     //////////////////BBBBAAAAAAAAAAAAAAADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD     but can use!
                    if (Firmware_Version.Data.Length >= 2)
                    {
                        UInt16 ttt = (UInt16)((Firmware_Version.Data[0] << 8) + Firmware_Version.Data[1]);
                        FirmwareView = ((ttt >> 11) + 2000) + "-" + ((ttt >> 7) & 15) + "-" + ((ttt >> 2) & 31) + "-" + (ttt & 3);
                    }
                    if(ID_L.Data.Length>=7 && ID_H.Data.Length >= 4)
                    {
                        IDView = "" + Convert.ToChar(ID_L.Data[0]) +
                            Convert.ToChar(ID_L.Data[1]) +
                            Convert.ToChar(ID_L.Data[2]) +
                            ((ID_L.Data[3] << 8) + ID_L.Data[4]).ToString().PadLeft(3, '0') +
                            ((ID_L.Data[5] << 8) + ID_L.Data[6]).ToString().PadLeft(3, '0') +
                            ID_H.Data[0].ToString().PadLeft(2, '0') +
                            ID_H.Data[1].ToString().PadLeft(2, '0') + '-' +
                            ((ID_H.Data[2] << 8) + ID_H.Data[3]).ToString().PadLeft(4, '0');
                    }
                }
            }
        }

        #region Laike USB CAN
        //Field
        static UInt32 m_DeviceHandle = 0;
        static UInt32 m_dwChannel = 0;
        //

        /// <summary>
        /// true = Connect success 
        /// false = Connect failed
        /// speed = 250K or 500K or 1000K
        /// </summary>
        private Boolean USB_CAN_Connect(String speed)
        {
            //先關閉
            CanCmd.CAN_ChannelStop(m_DeviceHandle, m_dwChannel);
            CanCmd.CAN_DeviceClose(m_DeviceHandle);

            char arg = '0';
            m_DeviceHandle = CanCmd.CAN_DeviceOpen(13, 0, ref arg);
            if (m_DeviceHandle == 0)
                return false;

            CAN_InitConfig config = new CAN_InitConfig();
            config.nFilter = (Byte)0;
            config.bMode = (Byte)0;
            config.dwAccCode = Convert.ToUInt32("00000000", 16);
            config.dwAccMask = Convert.ToUInt32("FFFFFFFF", 16);
            config.nBtrType = 1;   // 位定时参数模式(1表示SJA1000,0表示LPC21XX)
            //0014-1M  0016-800K  001C-500K  011C-250K  031C-125K  041C-100K  091C-50K  181C-20K  311C-10K  BFFF-5K
            if (speed == "500K")
            {
                config.dwBtr0 = 0x00;
                config.dwBtr1 = 0x1C;
            }
            else if (speed == "250K")
            {
                config.dwBtr0 = 0x01;
                config.dwBtr1 = 0x1C;
            }
            else if(speed == "1000K")
            {
                config.dwBtr0 = 0x00;
                config.dwBtr1 = 0x14;
            }

            if (CanCmd.CAN_RESULT_OK != CanCmd.CAN_ChannelStart(m_DeviceHandle, m_dwChannel, ref config))
                return false;
            return true;
        }

        /// <summary>
        /// true = Send success 
        /// false = Send failed
        /// ID = 00-00-00-00
        /// DATA = 00-00-00-00-00-00-00-00
        /// format =  0->Standard : 1->Extended
        /// length = 1~8
        /// </summary>
        unsafe public static Boolean USB_CAN_Send(string ID, string DATA, byte format, byte length)
        {
            CAN_DataFrame sendobj = new CAN_DataFrame();
            sendobj.nSendType = 1; //normal send
            sendobj.bRemoteFlag = 0; //data frame   1:remote frame
            sendobj.bExternFlag = format;
            sendobj.uID = Convert.ToUInt32(ID.Replace("-", ""), 16);
            sendobj.nDataLen = length;

            String[] stemp = DATA.Split('-');
            for (byte i = 0; i < length; i++)
                sendobj.arryData[i] = Convert.ToByte(stemp[i], 16);

            if (CanCmd.CAN_ChannelSend(m_DeviceHandle, m_dwChannel, ref sendobj, 1) == 0)
            {
                Console.WriteLine("Laike CAN_USB send Fail!");
                return false;
            }
            return true;
        }
        ////////
        unsafe public static Boolean USB_CAN_Send(string ID, byte[] DATA, byte format, byte length)
        {
            CAN_DataFrame sendobj = new CAN_DataFrame();
            sendobj.nSendType = 1; //normal send
            sendobj.bRemoteFlag = 0; //data frame   1:remote frame
            sendobj.bExternFlag = format;
            sendobj.uID = Convert.ToUInt32(ID.Replace("-", ""), 16);
            sendobj.nDataLen = length;

            for (byte i = 0; i < length; i++)
                sendobj.arryData[i] = DATA[i];

            if (CanCmd.CAN_ChannelSend(m_DeviceHandle, m_dwChannel, ref sendobj, 1) == 0)
            {
                Console.WriteLine("Laike CAN_USB send Fail!");
                return false;
            }
            return true;
        }

        //Receive
        unsafe private List<String> Receive()
        {
            UInt32 Rnum = CanCmd.CAN_GetReceiveCount(m_DeviceHandle, m_dwChannel);   //receive CAN Packageage numbers
            if (Rnum == 0) return null;

            UInt32 con_maxlen = 50;
            IntPtr pt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(CAN_DataFrame)) * (Int32)con_maxlen);
            Rnum = CanCmd.CAN_ChannelReceive(m_DeviceHandle, m_dwChannel, pt, con_maxlen, 100);

            List<String> data = new List<string>();
            if (Rnum != 0)
            {
                for (UInt32 i = 0; i < Rnum; i++)
                {
                    string temp = String.Empty;
                    CAN_DataFrame obj = (CAN_DataFrame)Marshal.PtrToStructure((IntPtr)((UInt32)pt + i * Marshal.SizeOf(typeof(CAN_DataFrame))), typeof(CAN_DataFrame));

                    //ID
                    temp += Convert.ToString((Int32)obj.uID, 16).ToUpper().PadLeft(8, '0');     //把ID補滿八位

                    temp += '-'; //ID與Data分隔號

                    //Data
                    String dd = String.Empty;
                    if (obj.bRemoteFlag == 0)
                    {
                        for (byte j = 0; j < obj.nDataLen; j++)
                            dd += Convert.ToString(obj.arryData[j], 16).PadLeft(2, '0');
                    }
                    dd.PadLeft(16, '0');

                    temp += dd;
                    data.Add(temp);
                }
            }
            else //error
            {
                CAN_ErrorInformation err = new CAN_ErrorInformation();  //error message
                if (CanCmd.CAN_GetErrorInfo(m_DeviceHandle, m_dwChannel, ref err) == CanCmd.CAN_RESULT_OK)
                    Console.WriteLine("Communication Error");
                else
                    Console.WriteLine("No data");
                return null;
            }

            Marshal.FreeHGlobal(pt);//RAM Release
            return data;
        }
        #endregion

        #region Timer
        private Timer timer_reconnect = new Timer();
        private Timer timer_log = new Timer();

        //Timer 初始化
        private void TimerStart()
        {
            timer_reconnect.Interval = 1000;
            timer_reconnect.Elapsed += new ElapsedEventHandler(Reconnect);
            timer_reconnect.Start();

            timer_log.Elapsed += new ElapsedEventHandler(LogFC);
        }

        public void Reconnect(object sender, System.Timers.ElapsedEventArgs e)
        {
            CONNECT = USB_CAN_Connect(CAN_SPEED_View);
            if (CONNECT)
                timer_reconnect.Stop();
            else
                Console.WriteLine("USB_CAN connect failed!");

            //try
            //{
            //    socket.Close();
            //    socket = new System.Net.Sockets.Socket(System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
            //    socket.Connect("127.0.0.1", 2502);
            //    CONNECT = true;
            //}
            //catch(Exception re)
            //{
            //    Console.WriteLine("Ethernet Connect Fail!");
            //    CONNECT = false;
            //}

            //if (CONNECT)
            //    timer_reconnect.Stop();
        }
        private String LogFilePathWithName = String.Empty;
        //private String LogFilePath = Environment.CurrentDirectory + @"\Log";//當前程式目錄\Log
        private String _LogFileName = "log";
        public String LogFileName { get { return _LogFileName; } set { _LogFileName = value;OnPropertyChanged(); } }
        private int LogSize = 0;
        public void LogFC(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (LogFileName[9] != DateTime.Now.ToString("yyyy-MM-dd")[9])//判斷是否跨日，暫時
            //    FILEname(LOG_PATH);

            StreamWriter sw = new StreamWriter(LogFilePathWithName, true);

            String Daa = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                        Battery.Soc + "," +
                        Battery.Voltage + "," +
                        Battery.Current + "," +
                        Battery.Status + "," +
                        Battery.Relay + ",";

            for (int i = 0; i < PACK; i++)
            {
                for (int j = 0; j < CELL ; j++)
                    Daa += Package[i].Voltage.cell[j] + ",";     //直接使用MODEL
                Daa += Package[i].Voltage.Total + "," + Package[i].Voltage.Min + "," + Package[i].Voltage.Max + ",";

                for (int j = 0; j < TEMP ; j++)
                    Daa += Package[i].Temperature.cell[j] + ",";    //直接使用MODEL
                Daa += Package[i].Temperature.Average + "," + Package[i].Temperature.Min + "," + Package[i].Temperature.Max + ",";
            }
            sw.WriteLine(Daa);

            //Log Size Control
            if (LogSize > 49998)
            {
                LogSize = 0;
                FILEname(LOG_PATH);
                //FILEname(Environment.CurrentDirectory + @"\Log");//同程式目錄底Log
                Console.WriteLine("50000 datas!\nChange file.");
            }
            LogSize++;
            sw.Close();
        }
        //FileName 
        public Boolean FILEname(string Path)
        {
            try
            {
                /////新增資料夾
                if (!Directory.Exists(Path))
                    Directory.CreateDirectory(Path);

                //LogFileName = DateTime.Now.ToString("yyyy-MM-dd") + "_1" + ".csv";
                //LogFilePathWithName = Path + @"\" + LogFileName;
                LogFilePathWithName = Path + @"\" + LogFileName + "-" + DateTime.Now.ToString("yyyy-MM-dd") + "_1" + ".csv";
                int k = 1;
                while (File.Exists(LogFilePathWithName))
                {
                    k++;
                    //LogFileName = DateTime.Now.ToString("yyyy-MM-dd") + "_" + k + ".csv";
                    //LogFilePathWithName = Path + @"\" + LogFileName;
                    LogFilePathWithName = Path + @"\" + LogFileName + "-" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + k + ".csv";
                }
                
                var sw = new StreamWriter(LogFilePathWithName, true);

                //Log Head
                string head = "TimeStamp,SOC,Voltage,Current,Status,Relay Status,";
                for (int i = 1; i < PACK + 1; i++)
                {
                    for (int j = 1; j < CELL + 1; j++)
                        head += "P" + i + "V.CELL-" + j + ",";
                    head += "P" + i + "V.Total," + "P" + i + "V.min," + "P" + i + "V.max,";

                    for (int j = 1; j < TEMP + 1; j++)
                        head += "P" + i + "T.CELL-" + j + ",";
                    head += "P" + i + "T.Average," + "P" + i + "T.min," + "P" + i + "T.max,";
                }
                sw.WriteLine(head);
                sw.Close();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Method FILEname Error!\n" + e.Message);
                return false;
            }
        }
        #endregion

        #region ICommand
        public ICommand ICommand_Exit { get { return new RelayCommand(Exit, CanExecute); } }
        public void Exit()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // 移除指定的appSettings
            config.AppSettings.Settings.Remove("CELL");
            config.AppSettings.Settings.Remove("PACK");
            config.AppSettings.Settings.Remove("TEMP");
            config.AppSettings.Settings.Remove("CAN_SPEED");
            config.AppSettings.Settings.Remove("LOG_PATH");
            config.AppSettings.Settings.Remove("Voltage_outlier");
            config.AppSettings.Settings.Remove("Voltage_high");
            config.AppSettings.Settings.Remove("Voltage_low");
            config.AppSettings.Settings.Remove("Temperature_high");
            config.AppSettings.Settings.Remove("Temperature_low"); 
            config.AppSettings.Settings.Remove("Log_Interval");
            // 新增指定的appSettings
            config.AppSettings.Settings.Add("CELL", CELL.ToString());
            config.AppSettings.Settings.Add("PACK", PACK.ToString());
            config.AppSettings.Settings.Add("TEMP", TEMP.ToString());
            config.AppSettings.Settings.Add("CAN_SPEED", CAN_SPEED);
            config.AppSettings.Settings.Add("LOG_PATH", LOG_PATH);
            config.AppSettings.Settings.Add("Voltage_outlier", voltage_outlier.ToString());
            config.AppSettings.Settings.Add("Voltage_high", voltage_high.ToString());
            config.AppSettings.Settings.Add("Voltage_low", voltage_low.ToString());
            config.AppSettings.Settings.Add("Temperature_high", temperature_high.ToString());
            config.AppSettings.Settings.Add("Temperature_low", temperature_low.ToString());
            config.AppSettings.Settings.Add("Log_Interval", Log_Interval.ToString());

            config.Save(ConfigurationSaveMode.Modified);

            //關閉Liake USB-CAN
            CanCmd.CAN_ChannelStop(m_DeviceHandle, m_dwChannel);
            CanCmd.CAN_DeviceClose(m_DeviceHandle);

            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~EXIT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        public ICommand ICommand_Log { get { return new RelayCommand(LogButton, CanExecute); } }
        public void LogButton()
        {
            if (LOG && CONNECT)
            {
                PathSelect();/////test
                timer_log.Interval = Log_Interval * 1000;      //設定Log間隔
                //FILEname(Environment.CurrentDirectory + @"\Log");   //開始Log前先刷新檔名
                FILEname(LOG_PATH);//開始Log前先刷新檔名
                timer_log.Start();
            }
            else
                timer_log.Stop();
        }
        public ICommand ICommand_EE_Read { get { return new RelayCommand(EE_Read, CanExecute); } }
        public void EE_Read()
        {
            byte[] ss = new byte[8];
            ss[0] = 0x52;
            ss[7] = 0xAE;
            USB_CAN_Send("18-00-EE-F0", ss, 1, 8);
        }
        public ICommand ICommand_EE_Write { get { return new RelayCommand(EE_Write, CanExecute); } }
        public void EE_Write()
        {
            foreach(EEPROM ee in OBS_EE)
            {
                if (ee.Change)
                {
                    byte[] send = new byte[8];
                    send[0] = ee.Index;
                    send[1] = ee.Len;
                    for (int i = 0; i < ee.Len; i++)
                        send[2 + i] = ee.Data[i];
                    for (int i = 0; i < 7; i++)
                        send[7] += send[i];
                    send[7] = (byte)(~send[7]);
                    String ss = BitConverter.ToString(send);
                    //Console.WriteLine(ss);
                    USB_CAN_Send("18-00-EE-F0",ss,1,8);
                }
                ee.Change = false;
            }
        }
        public ICommand ICommand_CalibrationVSOC { get { return new RelayCommand(CalibrationVSOC, CanExecute); } }
        public void CalibrationVSOC()
        {
            byte[] ss = new byte[8];
            ss[0] = 0xF0;
            ss[7] = 0x10;
            USB_CAN_Send("18-00-F4-F0", ss, 1, 8);
        }
        public ICommand ICommand_CalibrationCurrent { get { return new RelayCommand(CalibrationCurrent, CanExecute); } }
        public void CalibrationCurrent()
        {
            byte[] ss = new byte[8];
            ss[0] = 0xF1;
            ss[1] = CalibrationCur[0];
            ss[2] = CalibrationCur[1];
            ss[7] = (byte)~(ss[0] + ss[1] + ss[2]);
            USB_CAN_Send("18-00-F4-F0", ss, 1, 8);
        }

        public bool CanExecute()
        {
            return true;
        }
        #endregion

        public void PathSelect()
        {
            //File Select
            //var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            //var result = openFileDialog.ShowDialog();
            //if (result == true)
            //{
            //    var s = openFileDialog.FileName;
            //    Console.WriteLine(s);
            //}

            //Path Select
            var openBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = openBrowserDialog.ShowDialog();
            LOG_PATH = openBrowserDialog.SelectedPath;
        }

        public static int Hex2int(String hex)
        {
            int temp = 0;
            if (hex.Length % 2 != 0) return 0;
            for (int i = 0; i < hex.Length / 2; i++)
                temp += int.Parse(hex.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber) * (int)Math.Pow(256, i);
            return temp;
        }
    }
}
