﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_V3.Models
{
    class Voltage : ViewModelBase
    {
        public double[] cell = new double[12];
        private double max;
        private double min;
        private double total;
        private double dif;
        private bool dif_flag;

        public double Cell_1 { get { return cell[0]; } set { cell[0] = value; OnPropertyChanged(); } }
        public double Cell_2 { get { return cell[1]; } set { cell[1] = value; OnPropertyChanged(); } }
        public double Cell_3 { get { return cell[2]; } set { cell[2] = value; OnPropertyChanged(); } }
        public double Cell_4 { get { return cell[3]; } set { cell[3] = value; OnPropertyChanged(); } }
        public double Cell_5 { get { return cell[4]; } set { cell[4] = value; OnPropertyChanged(); } }
        public double Cell_6 { get { return cell[5]; } set { cell[5] = value; OnPropertyChanged(); } }
        public double Cell_7 { get { return cell[6]; } set { cell[6] = value; OnPropertyChanged(); } }
        public double Cell_8 { get { return cell[7]; } set { cell[7] = value; OnPropertyChanged(); } }
        public double Cell_9 { get { return cell[8]; } set { cell[8] = value; OnPropertyChanged(); } }
        public double Cell_10 { get { return cell[9]; } set { cell[9] = value; OnPropertyChanged(); } }
        public double Cell_11 { get { return cell[10]; } set { cell[10] = value; OnPropertyChanged(); } }
        public double Cell_12 { get { return cell[11]; } set { cell[11] = value; OnPropertyChanged(); } }
        public double Max { get { return max; } set { max = value; OnPropertyChanged(); } }
        public double Min { get { return min; } set { min = value; OnPropertyChanged(); } }
        public double Total { get { return total; } set { total = value; OnPropertyChanged(); } }
        public double Dif { get { return dif; } set { dif = value; OnPropertyChanged(); } }
        public bool Dif_flag { get { return dif_flag; } set { dif_flag = value; OnPropertyChanged(); } }
    }
}
