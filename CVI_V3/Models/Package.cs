﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_V3.Models
{
    class Package : ViewModelBase
    {
        private int index;
        private Voltage voltage = new Voltage();
        private Temperature temperature = new Temperature();

        public int Index { get { return index; } set { index = value;OnPropertyChanged(); } }
        public Voltage Voltage { get { return voltage; } set { voltage = value; OnPropertyChanged(); } }
        public Temperature Temperature { get { return temperature; } set { temperature = value;OnPropertyChanged(); } }

        public Package(int index)
        {
            this.index = index;
        }
    }
}
