﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_V3.Models
{
    class EEPROM : ViewModelBase
    {
        private byte _Index;
        private byte _Len;
        private byte[] _Data;
        private String _Name;
        private String _Unit;
        private bool _Change = false;

        public byte Index { get { return _Index; } set { _Index = value;OnPropertyChanged(); } }
        public byte Len { get { return _Len; } set { _Len = value;OnPropertyChanged(); } }
        public byte[] Data { get { return _Data; } set { _Data = value;OnPropertyChanged(); Change = true; } }
        public String Name { get { return _Name; } set { _Name = value;OnPropertyChanged(); } }
        public String Unit { get { return _Unit; } set { _Unit = value;OnPropertyChanged(); } }
        public bool Change { get { return _Change; } set { _Change = value; OnPropertyChanged(); } }

        public EEPROM(byte index, byte len,String name, String unit)
        {
            this._Index = index;
            this._Len = len;
            this._Name = name;
            this._Unit = unit;
            this._Data = new byte[len];
        }

        public void ClearChange()
        {
            this.Change = false;
        }


        //private double _Rated_Capacity;
        //private double _Nominal_Voltage;
        //private byte _Pack;
        //private byte _Cell;
        //private byte _Temp;
        //private byte _Current_Model;
        //private UInt16 _Firmware_Version;
        //private byte[] _Barcode;

        ////0x20
        //private double _OV_Tv_1;
        //private double _OV_Tv_2;
        //private double _OV_Tv_3;
        //private double _OV_Rv_1;
        //private double _OV_Rv_2;
        //private double _OV_Rv_3;
        //private byte _OV_Time_1;
        //private byte _OV_Time_2;
        //private byte _OV_Time_3;
        //private double _UV_Tv_1;
        //private double _UV_Tv_2;
        //private double _UV_Tv_3;
        //private double _UV_Rv_1;
        //private double _UV_Rv_2;
        //private double _UV_Rv_3;
        //private byte _UV_Time_1;
        //private byte _UV_Time_2;
        //private byte _UV_Time_3;

        ////0x40
        //private double _OC_CHG_Tv_1;
        //private double _OC_CHG_Tv_2;
        //private double _OC_CHG_Tv_3;
        //private byte _OC_CHG_Rt_1;
        //private byte _OC_CHG_Rt_2;
        //private byte _OC_CHG_Rt_3;
        //private byte _OC_CHG_Dt_1;
        //private byte _OC_CHG_Dt_2;
        //private byte _OC_CHG_Dt_3;
        //private double _OC_DSG_Tv_1;
        //private double _OC_DSG_Tv_2;
        //private double _OC_DSG_Tv_3;
        //private byte _OC_DSG_Rt_1;
        //private byte _OC_DSG_Rt_2;
        //private byte _OC_DSG_Rt_3;
        //private byte _OC_DSG_Dt_1;
        //private byte _OC_DSG_Dt_2;
        //private byte _OC_DSG_Dt_3;

        ////0x60
        //private double _OT_CHG_Tv_1;
        //private double _OT_CHG_Tv_2;
        //private double _OT_CHG_Tv_3;
        //private double _OT_CHG_Rv_1;
        //private double _OT_CHG_Rv_2;
        //private double _OT_CHG_Rv_3;
        //private byte _OT_CHG_Time_1;
        //private byte _OT_CHG_Time_2;
        //private byte _OT_CHG_Time_3;
        //private double _OT_DSG_Tv_1;
        //private double _OT_DSG_Tv_2;
        //private double _OT_DSG_Tv_3;
        //private double _OT_DSG_Rv_1;
        //private double _OT_DSG_Rv_2;
        //private double _OT_DSG_Rv_3;
        //private byte _OT_DSG_Time_1;
        //private byte _OT_DSG_Time_2;
        //private byte _OT_DSG_Time_3;
        //private double _UT_CHG_Tv_1;
        //private double _UT_CHG_Tv_2;
        //private double _UT_CHG_Tv_3;
        //private double _UT_CHG_Rv_1;
        //private double _UT_CHG_Rv_2;
        //private double _UT_CHG_Rv_3;
        //private byte _UT_CHG_Time_1;
        //private byte _UT_CHG_Time_2;
        //private byte _UT_CHG_Time_3;
        //private double _UT_DSG_Tv_1;
        //private double _UT_DSG_Tv_2;
        //private double _UT_DSG_Tv_3;
        //private double _UT_DSG_Rv_1;
        //private double _UT_DSG_Rv_2;
        //private double _UT_DSG_Rv_3;
        //private byte _UT_DSG_Time_1;
        //private byte _UT_DSG_Time_2;
        //private byte _UT_DSG_Time_3;

        ////0xA0
        //private double _SC_Tv;
        //private double _SC_Rt;
        //private byte _SC_DelayCount;
        //private double _Temp_UpperLimit;
        //private double _Temp_LowerLimit;
        ////0xB0
        //private double _ChargingVoltage;
        //private double _ChargingCurrent;
        //private double _FC_CHG_Tv;
        //private double _FC_CHG_Rv;
        //private byte _FC_CHG_Time;
        //private double _FD_CHG_Tv;
        //private double _FD_CHG_Rv;
        //private byte _FD_CHG_Time;
        //private UInt16 _Charged_State;
        ////0xC0
        //private double _PowerConsumption;
        //private byte _SOC;
        //private byte _SOH;
        //private UInt32 _Cumulative_Capability;
        //private UInt32 _Storage_Capability;
        //private UInt32 _Available_Capacity;
        ////0xD0
        //private double _Gain_1;
        //private double _Offset_1;
        //private double _Shunt_1;
        //private double _Reverse_current;
        //private double _Gain_2;
        //private double _Offset_2;
        //private double _Shunt_2;
        //private double _Ignored_current;
        ////0xE0
        //private UInt16 _OV_Count;
        //private UInt16 _UV_Count;
        //private UInt16 _OT_Count;
        //private UInt16 _UT_Count;
        //private UInt16 _OC_CHG_Count;
        //private UInt16 _OC_DSG_Count;
        //private UInt16 _SC_Count;
        //private UInt16 _Boot_Count;
        ////0xF0
        //private double _Cell_Voltage_Max;
        //private double _Cell_Voltage_Min;
        //private double _Cell_Temperature_Max;
        //private double _Cell_Temperature_Min;
        //private double _CHG_Current_Max;
        //private double _DSG_Current_Max;
        //private double _Cyclic_Capacity;
        //private UInt16 _CHG_Cycle;

        ////////////////////////////////////////////////////
        //public double Rated_Capacity { get { return _Rated_Capacity; } set { _Rated_Capacity = value;OnPropertyChanged(); } }
        //public double Nominal_Voltage { get { return _Nominal_Voltage; } set { _Nominal_Voltage = value;OnPropertyChanged(); } }
        //public byte Pack { get { return _Pack; } set { _Pack = value; OnPropertyChanged(); } }
        //public byte Cell { get { return _Cell; } set { _Cell = value; OnPropertyChanged(); } }
        //public byte Temp { get { return _Temp; } set { _Temp = value; OnPropertyChanged(); } }
        //public byte Current_Model { get { return _Current_Model; } set { _Current_Model = value; OnPropertyChanged(); } }
        //public UInt16 Firmware_Version { get { return _Firmware_Version; } set { _Firmware_Version = value; OnPropertyChanged(); } }
        //public byte[] Barcode { get { return _Barcode; } set { _Barcode = value; OnPropertyChanged(); } }
        ////0x20
        //public double OV_Tv_1 { get { return _OV_Tv_1; } set { _OV_Tv_1 = value; OnPropertyChanged(); } }
        //public double OV_Tv_2 { get { return _OV_Tv_2; } set { _OV_Tv_2 = value; OnPropertyChanged(); } }
        //public double OV_Tv_3 { get { return _OV_Tv_3; } set { _OV_Tv_3 = value; OnPropertyChanged(); } }
        //public double OV_Rv_1 { get { return _OV_Rv_1; } set { _OV_Rv_1 = value; OnPropertyChanged(); } }
        //public double OV_Rv_2 { get { return _OV_Rv_2; } set { _OV_Rv_2 = value; OnPropertyChanged(); } }
        //public double OV_Rv_3 { get { return _OV_Rv_3; } set { _OV_Rv_3 = value; OnPropertyChanged(); } }
        //public byte OV_Time_1 { get { return _OV_Time_1; } set { _OV_Time_1 = value; OnPropertyChanged(); } }
        //public byte OV_Time_2 { get { return _OV_Time_2; } set { _OV_Time_2 = value; OnPropertyChanged(); } }
        //public byte OV_Time_3 { get { return _OV_Time_3; } set { _OV_Time_3 = value; OnPropertyChanged(); } }
        //public double UV_Tv_1 { get { return _UV_Tv_1; } set { _UV_Tv_1 = value; OnPropertyChanged(); } }
        //public double UV_Tv_2 { get { return _UV_Tv_2; } set { _UV_Tv_2 = value; OnPropertyChanged(); } }
        //public double UV_Tv_3 { get { return _UV_Tv_3; } set { _UV_Tv_3 = value; OnPropertyChanged(); } }
        //public double UV_Rv_1 { get { return _UV_Rv_1; } set { _UV_Rv_1 = value; OnPropertyChanged(); } }
        //public double UV_Rv_2 { get { return _UV_Rv_2; } set { _UV_Rv_2 = value; OnPropertyChanged(); } }
        //public double UV_Rv_3 { get { return _UV_Rv_3; } set { _UV_Rv_3 = value; OnPropertyChanged(); } }
        //public byte UV_Time_1 { get { return _UV_Time_1; } set { _UV_Time_1 = value; OnPropertyChanged(); } }
        //public byte UV_Time_2 { get { return _UV_Time_2; } set { _UV_Time_2 = value; OnPropertyChanged(); } }
        //public byte UV_Time_3 { get { return _UV_Time_3; } set { _UV_Time_3 = value; OnPropertyChanged(); } }

        ////0x40
        //public double OC_CHG_Tv_1 { get { return _OC_CHG_Tv_1; } set { _OC_CHG_Tv_1 = value; OnPropertyChanged(); } }
        //public double OC_CHG_Tv_2 { get { return _OC_CHG_Tv_2; } set { _OC_CHG_Tv_2 = value; OnPropertyChanged(); } }
        //public double OC_CHG_Tv_3 { get { return _OC_CHG_Tv_3; } set { _OC_CHG_Tv_3 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Rt_1 { get { return _OC_CHG_Rt_1; } set { _OC_CHG_Rt_1 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Rt_2 { get { return _OC_CHG_Rt_2; } set { _OC_CHG_Rt_2 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Rt_3 { get { return _OC_CHG_Rt_3; } set { _OC_CHG_Rt_3 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Dt_1 { get { return _OC_CHG_Dt_1; } set { _OC_CHG_Dt_1 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Dt_2 { get { return _OC_CHG_Dt_2; } set { _OC_CHG_Dt_2 = value; OnPropertyChanged(); } }
        //public byte OC_CHG_Dt_3 { get { return _OC_CHG_Dt_3; } set { _OC_CHG_Dt_3 = value; OnPropertyChanged(); } }
        //public double OC_DSG_Tv_1 { get { return _OC_DSG_Tv_1; } set { _OC_DSG_Tv_1 = value; OnPropertyChanged(); } }
        //public double OC_DSG_Tv_2 { get { return _OC_DSG_Tv_2; } set { _OC_DSG_Tv_2 = value; OnPropertyChanged(); } }
        //public double OC_DSG_Tv_3 { get { return _OC_DSG_Tv_3; } set { _OC_DSG_Tv_3 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Rt_1 { get { return _OC_DSG_Rt_1; } set { _OC_DSG_Rt_1 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Rt_2 { get { return _OC_DSG_Rt_2; } set { _OC_DSG_Rt_2 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Rt_3 { get { return _OC_DSG_Rt_3; } set { _OC_DSG_Rt_3 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Dt_1 { get { return _OC_DSG_Dt_1; } set { _OC_DSG_Dt_1 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Dt_2 { get { return _OC_DSG_Dt_2; } set { _OC_DSG_Dt_2 = value; OnPropertyChanged(); } }
        //public byte OC_DSG_Dt_3 { get { return _OC_DSG_Dt_3; } set { _OC_DSG_Dt_3 = value; OnPropertyChanged(); } }

        ////0x60
        //public double OT_CHG_Tv_1 { get { return _OT_CHG_Tv_1; } set { _OT_CHG_Tv_1 = value; OnPropertyChanged(); } }
        //public double OT_CHG_Tv_2 { get { return _OT_CHG_Tv_2; } set { _OT_CHG_Tv_2 = value; OnPropertyChanged(); } }
        //public double OT_CHG_Tv_3 { get { return _OT_CHG_Tv_3; } set { _OT_CHG_Tv_3 = value; OnPropertyChanged(); } }
        //public double OT_CHG_Rv_1 { get { return _OT_CHG_Rv_1; } set { _OT_CHG_Rv_1 = value; OnPropertyChanged(); } }
        //public double OT_CHG_Rv_2 { get { return _OT_CHG_Rv_2; } set { _OT_CHG_Rv_2 = value; OnPropertyChanged(); } }
        //public double OT_CHG_Rv_3 { get { return _OT_CHG_Rv_3; } set { _OT_CHG_Rv_3 = value; OnPropertyChanged(); } }
        //public byte OT_CHG_Time_1 { get { return _OT_CHG_Time_1; } set { _OT_CHG_Time_1 = value; OnPropertyChanged(); } }
        //public byte OT_CHG_Time_2 { get { return _OT_CHG_Time_2; } set { _OT_CHG_Time_2 = value; OnPropertyChanged(); } }
        //public byte OT_CHG_Time_3 { get { return _OT_CHG_Time_3; } set { _OT_CHG_Time_3 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Tv_1 { get { return _OT_DSG_Tv_1; } set { _OT_DSG_Tv_1 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Tv_2 { get { return _OT_DSG_Tv_2; } set { _OT_DSG_Tv_2 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Tv_3 { get { return _OT_DSG_Tv_3; } set { _OT_DSG_Tv_3 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Rv_1 { get { return _OT_DSG_Rv_1; } set { _OT_DSG_Rv_1 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Rv_2 { get { return _OT_DSG_Rv_2; } set { _OT_DSG_Rv_2 = value; OnPropertyChanged(); } }
        //public double OT_DSG_Rv_3 { get { return _OT_DSG_Rv_3; } set { _OT_DSG_Rv_3 = value; OnPropertyChanged(); } }
        //public byte OT_DSG_Time_1 { get { return _OT_DSG_Time_1; } set { _OT_DSG_Time_1 = value; OnPropertyChanged(); } }
        //public byte OT_DSG_Time_2 { get { return _OT_DSG_Time_2; } set { _OT_DSG_Time_2 = value; OnPropertyChanged(); } }
        //public byte OT_DSG_Time_3 { get { return _OT_DSG_Time_3; } set { _OT_DSG_Time_3 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Tv_1 { get { return _UT_CHG_Tv_1; } set { _UT_CHG_Tv_1 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Tv_2 { get { return _UT_CHG_Tv_2; } set { _UT_CHG_Tv_2 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Tv_3 { get { return _UT_CHG_Tv_3; } set { _UT_CHG_Tv_3 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Rv_1 { get { return _UT_CHG_Rv_1; } set { _UT_CHG_Rv_1 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Rv_2 { get { return _UT_CHG_Rv_2; } set { _UT_CHG_Rv_2 = value; OnPropertyChanged(); } }
        //public double UT_CHG_Rv_3 { get { return _UT_CHG_Rv_3; } set { _UT_CHG_Rv_3 = value; OnPropertyChanged(); } }
        //public byte UT_CHG_Time_1 { get { return _UT_CHG_Time_1; } set { _UT_CHG_Time_1 = value; OnPropertyChanged(); } }
        //public byte UT_CHG_Time_2 { get { return _UT_CHG_Time_2; } set { _UT_CHG_Time_2 = value; OnPropertyChanged(); } }
        //public byte UT_CHG_Time_3 { get { return _UT_CHG_Time_3; } set { _UT_CHG_Time_3 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Tv_1 { get { return _UT_DSG_Tv_1; } set { _UT_DSG_Tv_1 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Tv_2 { get { return _UT_DSG_Tv_2; } set { _UT_DSG_Tv_2 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Tv_3 { get { return _UT_DSG_Tv_3; } set { _UT_DSG_Tv_3 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Rv_1 { get { return _UT_DSG_Rv_1; } set { _UT_DSG_Rv_1 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Rv_2 { get { return _UT_DSG_Rv_2; } set { _UT_DSG_Rv_2 = value; OnPropertyChanged(); } }
        //public double UT_DSG_Rv_3 { get { return _UT_DSG_Rv_3; } set { _UT_DSG_Rv_3 = value; OnPropertyChanged(); } }
        //public byte UT_DSG_Time_1 { get { return _UT_DSG_Time_1; } set { _UT_DSG_Time_1 = value; OnPropertyChanged(); } }
        //public byte UT_DSG_Time_2 { get { return _UT_DSG_Time_2; } set { _UT_DSG_Time_2 = value; OnPropertyChanged(); } }
        //public byte UT_DSG_Time_3 { get { return _UT_DSG_Time_3; } set { _UT_DSG_Time_3 = value; OnPropertyChanged(); } }

        ////0xA0
        //public double SC_Tv { get { return _SC_Tv; } set { _SC_Tv = value; OnPropertyChanged(); } }
        //public double SC_Rt { get { return _SC_Rt; } set { _SC_Rt = value; OnPropertyChanged(); } }
        //public byte SC_DelayCount { get { return _SC_DelayCount; } set { _SC_DelayCount = value; OnPropertyChanged(); } }
        //public double Temp_UpperLimit { get { return _Temp_UpperLimit; } set { _Temp_UpperLimit = value; OnPropertyChanged(); } }
        //public double Temp_LowerLimit { get { return _Temp_LowerLimit; } set { _Temp_LowerLimit = value; OnPropertyChanged(); } }
        ////0xB0
        //public double ChargingVoltage { get { return _ChargingVoltage; } set { _ChargingVoltage = value; OnPropertyChanged(); } }
        //public double ChargingCurrent { get { return _ChargingCurrent; } set { _ChargingCurrent = value; OnPropertyChanged(); } }
        //public double FC_CHG_Tv { get { return _FC_CHG_Tv; } set { _FC_CHG_Tv = value; OnPropertyChanged(); } }
        //public double FC_CHG_Rv { get { return _FC_CHG_Rv; } set { _FC_CHG_Rv = value; OnPropertyChanged(); } }
        //public byte FC_CHG_Time { get { return _FC_CHG_Time; } set { _FC_CHG_Time = value; OnPropertyChanged(); } }
        //public double FD_CHG_Tv { get { return _FD_CHG_Tv; } set { _FD_CHG_Tv = value; OnPropertyChanged(); } }
        //public double FD_CHG_Rv { get { return _FD_CHG_Rv; } set { _FD_CHG_Rv = value; OnPropertyChanged(); } }
        //public byte FD_CHG_Time { get { return _FD_CHG_Time; } set { _FD_CHG_Time = value; OnPropertyChanged(); } }
        //public UInt16 Charged_State { get { return _Charged_State; } set { _Charged_State = value; OnPropertyChanged(); } }
        ////0xC0
        //public double PowerConsumption { get { return _PowerConsumption; } set { _PowerConsumption = value; OnPropertyChanged(); } }
        //public byte SOC { get { return _SOC; } set { _SOC = value; OnPropertyChanged(); } }
        //public byte SOH { get { return _SOH; } set { _SOH = value; OnPropertyChanged(); } }
        //public UInt32 Cumulative_Capability { get { return _Cumulative_Capability; } set { _Cumulative_Capability = value; OnPropertyChanged(); } }
        //public UInt32 Storage_Capability { get { return _Storage_Capability; } set { _Storage_Capability = value; OnPropertyChanged(); } }
        //public UInt32 Available_Capacity { get { return _Available_Capacity; } set { _Available_Capacity = value; OnPropertyChanged(); } }
        ////0xD0
        //public double Gain_1 { get { return _Gain_1; } set { _Gain_1 = value; OnPropertyChanged(); } }
        //public double Offset_1 { get { return _Offset_1; } set { _Offset_1 = value; OnPropertyChanged(); } }
        //public double Shunt_1 { get { return _Shunt_1; } set { _Shunt_1 = value; OnPropertyChanged(); } }
        //public double Reverse_current { get { return _Reverse_current; } set { _Reverse_current = value; OnPropertyChanged(); } }
        //public double Gain_2 { get { return _Gain_2; } set { _Gain_2 = value; OnPropertyChanged(); } }
        //public double Offset_2 { get { return _Offset_2; } set { _Offset_2 = value; OnPropertyChanged(); } }
        //public double Shunt_2 { get { return _Shunt_2; } set { _Shunt_2 = value; OnPropertyChanged(); } }
        //public double Ignored_current { get { return _Ignored_current; } set { _Ignored_current = value; OnPropertyChanged(); } }
        ////0xE0
        //public UInt16 OV_Count { get { return _OV_Count; } set { _OV_Count = value; OnPropertyChanged(); } }
        //public UInt16 UV_Count { get { return _UV_Count; } set { _UV_Count = value; OnPropertyChanged(); } }
        //public UInt16 OT_Count { get { return _OT_Count; } set { _OT_Count = value; OnPropertyChanged(); } }
        //public UInt16 UT_Count { get { return _UT_Count; } set { _UT_Count = value; OnPropertyChanged(); } }
        //public UInt16 OC_CHG_Count { get { return _OC_CHG_Count; } set { _OC_CHG_Count = value; OnPropertyChanged(); } }
        //public UInt16 OC_DSG_Count { get { return _OC_DSG_Count; } set { _OC_DSG_Count = value; OnPropertyChanged(); } }
        //public UInt16 SC_Count { get { return _SC_Count; } set { _SC_Count = value; OnPropertyChanged(); } }
        //public UInt16 Boot_Count { get { return _Boot_Count; } set { _Boot_Count = value; OnPropertyChanged(); } }
        ////0xF0
        //public double Cell_Voltage_Max { get { return _Cell_Voltage_Max; } set { _Cell_Voltage_Max = value; OnPropertyChanged(); } }
        //public double Cell_Voltage_Min { get { return _Cell_Voltage_Min; } set { _Cell_Voltage_Min = value; OnPropertyChanged(); } }
        //public double Cell_Temperature_Max { get { return _Cell_Temperature_Max; } set { _Cell_Temperature_Max = value; OnPropertyChanged(); } }
        //public double Cell_Temperature_Min { get { return _Cell_Temperature_Min; } set { _Cell_Temperature_Min = value; OnPropertyChanged(); } }
        //public double CHG_Current_Max { get { return _CHG_Current_Max; } set { _CHG_Current_Max = value; OnPropertyChanged(); } }
        //public double DSG_Current_Max { get { return _DSG_Current_Max; } set { _DSG_Current_Max = value; OnPropertyChanged(); } }
        //public double Cyclic_Capacity { get { return _Cyclic_Capacity; } set { _Cyclic_Capacity = value; OnPropertyChanged(); } }
        //public UInt16 CHG_Cycle { get { return _CHG_Cycle; } set { _CHG_Cycle = value; OnPropertyChanged(); } }

    }
}
