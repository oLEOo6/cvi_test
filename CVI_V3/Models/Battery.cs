﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_V3.Models
{
    class Battery : ViewModelBase
    {
        private double voltage;
        private double current;
        private double temperature;
        private byte soc;
        private UInt16 status;
        private byte errorcode;
        private byte relay;
        private UInt16 BMSversion;
        private UInt16 FIRMWAREversion;

        public double Voltage { get { return voltage; } set { voltage = value;OnPropertyChanged(); } }
        public double Current { get { return current; } set { current = value; OnPropertyChanged(); } }
        public double Temperature { get { return temperature; } set { temperature = value; OnPropertyChanged(); } }
        public byte Soc { get { return soc; } set { soc = value; OnPropertyChanged(); } }
        public UInt16 Status { get { return status; } set { status = value; OnPropertyChanged(); } }
        public byte Errorcode { get { return errorcode; } set { errorcode = value; OnPropertyChanged(); } }
        public byte Relay { get { return relay; } set { relay = value; OnPropertyChanged(); } }
        public UInt16 BMSVersion { get { return BMSversion; } set { BMSversion = value; OnPropertyChanged(); } }
        public UInt16 FIRMWAREVersion { get { return FIRMWAREversion; } set { FIRMWAREversion = value; OnPropertyChanged(); } }
    }
}
