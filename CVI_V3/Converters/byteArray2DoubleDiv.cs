﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CVI_V3.Converters
{
    class byteArray2DoubleDiv : IValueConverter
    {
        //parameter-->-->-->Unsigned(U) or Signed(S)-byte.Length-DivNumber     ex:U-2-100
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte[] temp = (byte[])value;
            String[] pa = ((String)parameter).Split('-');
            int len = System.Convert.ToInt32(pa[1]);
            double div = System.Convert.ToDouble(pa[2]);

            int tt = 0;
            for(int i = 0; i < len; i++)
                tt += (temp[i] << (8 * i));

            if (len == 2)
            {
                if (pa[0] == "U")
                    return (UInt16)tt / div;
                else if (pa[0] == "S")
                    return (Int16)tt / div;
            }
            else if(len == 4)
            {
                if (pa[0] == "U")
                    return (UInt32)tt / div;
                else if (pa[0] == "S")
                    return (Int32)tt / div;
            }
            else if(len == 1)
            {
                if (pa[0] == "U")
                    return (byte)tt / div;
                else if (pa[0] == "S")
                    return (SByte)tt / div;
            }
            
            return 87.87;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double temp = System.Convert.ToDouble(value);
            String[] pa = ((String)parameter).Split('-');
            int len = System.Convert.ToInt32(pa[1]);
            double div = System.Convert.ToDouble(pa[2]);

            temp *= div;
            if(len == 2)
            {
                if (pa[0] == "U")
                    return BitConverter.GetBytes(System.Convert.ToUInt16(temp));
                else if(pa[0] == "S")
                    return BitConverter.GetBytes(System.Convert.ToInt16(temp));
            }
            else if(len == 4)
            {
                if (pa[0] == "U")
                    return BitConverter.GetBytes(System.Convert.ToUInt32(temp));
                else if (pa[0] == "S")
                    return BitConverter.GetBytes(System.Convert.ToInt32(temp));
            }
            else if(len == 1)
            {
                if (pa[0] == "U")
                    return BitConverter.GetBytes(System.Convert.ToByte(temp));
                else if (pa[0] == "S")
                    return BitConverter.GetBytes(System.Convert.ToSByte(temp));
            }
            Console.WriteLine("DEWork");
            
            return new byte[] { 87 };
        }
    }
}
