﻿using CVI_V3.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_V3.Views
{
    /// <summary>
    /// HomeView.xaml 的互動邏輯
    /// </summary>
    public partial class HomeView : UserControl
    {
        public HomeView()
        {
            InitializeComponent();

            datagrid_voltage.Columns.Add(new DataGridTextColumn
            {
                Header = "Index",
                Binding = new Binding("Index"),
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_temperature.Columns.Add(new DataGridTextColumn
            {
                Header = "Index",
                Binding = new Binding("Index"),
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });

            for (int i = 1; i < MainWindowVM.CELL + 1; i++)
            {
                string Ctmp = "Cell__" + i;
                string Vtmp = "Voltage.Cell_" + i;     //WOW
                datagrid_voltage.Columns.Add(new DataGridTextColorColumn_V 
                { 
                    Header = Ctmp,
                    Binding = new Binding(Vtmp) { StringFormat = "{0:F2}" }, 
                    IsReadOnly = true, 
                    //CanUserSort = false, 
                    FontSize = 16, 
                    FontWeight = FontWeights.Bold 
                });
            }
            for (int i = 1; i < MainWindowVM.TEMP + 1; i++)
            {
                string Ctmp = "Cell__" + i;
                string Ttmp = "Temperature.Cell_" + i;     //WOW
                datagrid_temperature.Columns.Add(new DataGridTextColorColumn_T 
                { 
                    Header = Ctmp, 
                    Binding = new Binding(Ttmp), 
                    IsReadOnly = true, 
                    //CanUserSort = false, 
                    FontSize = 16, 
                    FontWeight = FontWeights.Bold 
                });
            }
            datagrid_voltage.Columns.Add(new DataGridTextColumn
            {
                Header = "Total",
                Binding = new Binding("Voltage.Total") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_voltage.Columns.Add(new DataGridTextColumn
            {
                Header = "Min",
                Binding = new Binding("Voltage.Min") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_voltage.Columns.Add(new DataGridTextColumn
            {
                Header = "Max",
                Binding = new Binding("Voltage.Max") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_voltage.Columns.Add(new DataGridTextColumn
            {
                Header = "Dif",
                Binding = new Binding("Voltage.Dif") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_voltage.Columns.Add(new DataGridCheckBoxColumn
            {
                Header = "Dif__Flag",
                Binding = new Binding("Voltage.Dif_flag"),
                //CanUserSort = false
            });


            datagrid_temperature.Columns.Add(new DataGridTextColumn
            {
                Header = "Average",
                Binding = new Binding("Temperature.Average") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_temperature.Columns.Add(new DataGridTextColumn
            {
                Header = "Min",
                Binding = new Binding("Temperature.Min"),
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_temperature.Columns.Add(new DataGridTextColumn
            {
                Header = "Max",
                Binding = new Binding("Temperature.Max"),
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            });
            datagrid_temperature.Columns.Add(new DataGridTextColumn
            {
                Header = "Dif",
                Binding = new Binding("Temperature.Dif") { StringFormat = "{0:F2}" },
                IsReadOnly = true,
                //CanUserSort = false,
                FontSize = 16,
                FontWeight = FontWeights.Bold
            }); ;
        }
    }

    #region DataColumn
    //電壓
    public class DataGridTextColorColumn_V : DataGridTextColumn
    {
        static Value2ColorConverter_V cv = new Value2ColorConverter_V();
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            //GenerateElement可以用來任意組裝要呈現的元素, 很有彈性
            TextBlock tb = base.GenerateElement(cell, dataItem) as TextBlock;
            Binding b = new Binding("Text") { Converter = cv, RelativeSource = RelativeSource.Self };
            tb.SetBinding(TextBlock.ForegroundProperty, b);
            tb.TextAlignment = TextAlignment.Center; //文字置中
            return tb;
        }
    }

    //溫度
    public class DataGridTextColorColumn_T : DataGridTextColumn
    {
        static Value2ColorConverter_T cv = new Value2ColorConverter_T();
        protected override FrameworkElement GenerateElement(DataGridCell cell, object dataItem)
        {
            //GenerateElement可以用來任意組裝要呈現的元素, 很有彈性
            TextBlock tb = base.GenerateElement(cell, dataItem) as TextBlock;
            Binding b = new Binding("Text") { Converter = cv, RelativeSource = RelativeSource.Self };
            tb.SetBinding(TextBlock.ForegroundProperty, b);
            tb.TextAlignment = TextAlignment.Center; //文字置中
            return tb;
        }
    }

    //Single Cell Voltage
    class Value2ColorConverter_V : IValueConverter
    {
        public static double max = MainWindowVM.voltage_high;
        public static double min = MainWindowVM.voltage_low;
        public SolidColorBrush UN = new SolidColorBrush(Colors.Red);
        public SolidColorBrush N = new SolidColorBrush(Colors.Green);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == "") return new SolidColorBrush(Colors.White);
            double temp = Double.Parse((string)value);

            if (temp < min || temp > max)
                return UN;
            return N;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    //Single Cell Temperature
    class Value2ColorConverter_T : IValueConverter
    {
        public static double max = MainWindowVM.temperature_high;
        public static double min = MainWindowVM.temperature_low;
        public SolidColorBrush UN = new SolidColorBrush(Colors.Red);
        public SolidColorBrush N = new SolidColorBrush(Colors.Green);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == "") return new SolidColorBrush(Colors.White);
            double temp = Double.Parse((string)value);

            if (temp < min || temp > max)
                return UN;
            return N;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
    #endregion
}
