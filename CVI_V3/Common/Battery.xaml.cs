﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CVI_V3.Common
{
    /// <summary>
    /// Battery.xaml 的互動邏輯
    /// </summary>
    public partial class Battery : UserControl , INotifyPropertyChanged
    {
        public Battery()
        {
            InitializeComponent();
        }

        //public int 

        #region DependencyProperty
        public int Voltage
        {
            get { return (int)GetValue(VoltageProperty); }
            set { SetValue(VoltageProperty, value); }
        }
        public static readonly DependencyProperty VoltageProperty = DependencyProperty.Register("Voltage", typeof(int), typeof(Battery));

        public int MaxVoltage
        {
            get { return (int)GetValue(MaxVoltageProperty); }
            set { SetValue(MaxVoltageProperty, value); }
        }
        public static readonly DependencyProperty MaxVoltageProperty = DependencyProperty.Register("MaxVoltage", typeof(int), typeof(Battery));

        public int MinVoltage
        {
            get { return (int)GetValue(MinVoltageProperty); }
            set { SetValue(MinVoltageProperty, value); }
        }
        public static readonly DependencyProperty MinVoltageProperty = DependencyProperty.Register("MinVoltage", typeof(int), typeof(Battery));
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
